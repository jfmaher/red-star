<?php
require_once( "config.php" );
require_once( "user.class.php");
require_once( "profile.class.php");
require_once( "pbkdf2.php" );

/*
*   Function that displays the standard nav bar along the side of the page
*   If the current user is logged in and an admin user, the admin menu is displayed.
*/
function displayNavBar()  {
    ?>
      <div class="n">
      <div class="sitemap">
        <h2>Site Map</h2>
        <div class="site-menu">
          <ul class="site-menu-1 site-menu">
          <li><a href="profile.php">Profile</a></li>
          <li><a href="groups.php">Group Managment</a></li>
          <li><a href="makeBooking.php">Make a booking</a></li>
          <li><a href="confirm.php">Confirm a booking</a></li>
          <li><a href="bookingList.php">View bookings</a></li>
          <li><a href="logout.php">Log out</a></li>
          <?php
            if(isLoggedIn() && isAdminAccount($_SESSION["user"]->getValue( "username" )))                 // Checks to see if someone is logged in and then if they ar an admin
            {
              ?>
              <li class="menu open">
                <a class="sitemap-toggle sitemap-toggle-reset sitemap-toggle-toggled" tabindex="0" style="height: 16px;"><span class="sitemap-toggle-char">−</span></a>
                <a href="AdminAccounts.php">Admin</a>
                <ul>
                  <li><a href="AdminAccounts.php">View Accounts</a></li>
                  <li><a href="displayeverybooking.php">View Bookings</a></li>
                  <li><a href="Adminheatmap.php">View Heatmap</a></li>
                  <li><a href="addUser.php">Add New User</a></li>
                  <li><a href="blacklist_user.php">Blacklist User</a></li>
                </ul>
              </li>
            <?php
          }
        ?>
        </ul>
        </div>
      </div>
    </div>
    <?php
}

/*
*   Function that displays the Doctype declaration.
*   The variable passed, is the title of the page.
*/
function insertDoctypeDeclaration($title)
{
?>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
              <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
              <title><?php echo $title ?></title>
<?php
}

/*
*     If there are any standard scripts, they would be inserted into this function
*/ 
function insertStandardScripts()
{
}

/*
*   Function that inserts the scripts used to display the javascript calendar for selecting dates.
*/
function insertCalendarScripts()
{
  ?>
  <script type="text/javascript" src="scripts/jsDatePick.min.1.3.js"></script>

  <script type="text/javascript">
  window.onload = function(){
    
    
    g_globalObject = new JsDatePick({
      useMode:1,
      isStripped:true,
      target:"div3_example"
    });   
    
    g_globalObject.setOnSelectedDelegate(function(){
      var obj = g_globalObject.getSelectedDay();
      document.forms["test"].year.value = obj.year
      document.forms["test"].month.value = obj.month
      document.forms["test"].day.value = obj.day
      /*alert (document.forms["test"].year.value + "-" + document.forms["test"].month.value + "-" + document.forms["test"].day.value);*/
      /*document.getElementById("div3_example_result").innerHTML = obj.day + "/" + obj.month + "/" + obj.year;*/
    });
    
    
    
  /*  
    g_globalObject2 = new JsDatePick({
      useMode:1,
      isStripped:false,
      target:"div4_example",
      cellColorScheme:"beige"
    });
    
    g_globalObject2.setOnSelectedDelegate(function(){
      var obj = g_globalObject2.getSelectedDay();
      alert("a date was just selected and the date is : " + obj.day + "/" + obj.month + "/" + obj.year);
      document.getElementById("div3_example_result").innerHTML = obj.day + "/" + obj.month + "/" + obj.year;
    });   
    */
  };
</script>
  <?php
}

/*
*     Function that inserts the standard sytle sheets onto the webpage.
*/
function insertStandardStyleSheets()
{
  ?>
  <link rel="Stylesheet" type="text/css" href="//www.scss.tcd.ie/tms/m/styles/master.css" media="screen">
  <link rel="Stylesheet" type="text/css" href="//www.scss.tcd.ie/tms/m/styles/print.css" media="print">
  <link rel="Stylesheet" type="text/css" href="//www.scss.tcd.ie/tms/t/scss/styles-scss.css?v=4" media="screen">
  <link rel="stylesheet" type="text/css" href="//www.scss.tcd.ie/tms/m/scripts/jquery/shadowbox/3.0.3/shadowbox.css">
  <link rel="stylesheet" href="videos/me.js/mediaelementplayer.css">
<?php
}

/*
*     Function that inserts the stlesheets used by the javascript calendar
*/
function insertCalendarStyleSheets()
{
  ?>
  <link rel="stylesheet" type="text/css" media="all" href="cal/jsDatePick_ltr.min.css" />
  <?php
}


/*
*     Function that displays the standard page markup. 
*     The $title variable is used to change the banner text.
*/
function insertStandardBody($title)
{
  ?>
  </head>
        <body>
          <div class="wrap">

    <div class="core-header">
      <h1>
      <a href="//www.tcd.ie">
        Trinity College Dublin
        <span></span>
      </a>
    </h1> 
    <form class="core-search" method="get" action="http://search.tcd.ie/search">
                <fieldset>
                        <legend><span class="accelerator-key">S</span>earch TCD</legend>
                        <label for="search" class="cloak">Your query: </label>
                        <input accesskey="s" type="text" name="q" id="search" value="Search TCD" onfocus="if(this.value=='Search TCD'){this.value='';this.style.color='#54575e';this.style.background='#ffffff';}else{this.select();}" onblur="if(this.value==''){this.value='Search TCD';this.style.color='#54575e';this.style.background='#EDF1F8';}">
                        <input type="image" src="//www.scss.tcd.ie/tms/m/styles/img/go.gif" class="submit" alt="Go" value="go">
                        <input type="hidden" name="entqr" value="0">
                        <input type="hidden" name="ud" value="1">
                        <input type="hidden" name="sort" value="date:D:L:d1">
                        <input type="hidden" name="output" value="xml_no_dtd">
                        <input type="hidden" name="oe" value="UTF-8">
                        <input type="hidden" name="ie" value="ISO-8859-1">
                        <input type="hidden" name="client" value="default_frontend">
                        <input type="hidden" name="proxystylesheet" value="default_frontend">
                        <input type="hidden" name="site" value="default_collection">
                </fieldset>
    </form>
    <h2 class="cloak">Top Level TCD Links</h2>
    <div class="core-tabs">
        <ul>                        
          <li><a href="//www.tcd.ie/" class="core-tab" title="TCD Home"><span>TCD Home</span></a></li>
          <li id="selected" title="Visit Faculties &amp; Schools"><a href="//www.tcd.ie/structure/" title="Visit Faculties &amp; Schools"><span>Faculties &amp; Schools</span></a></li>
          <li><a href="//www.tcd.ie/courses/" title="Browse TCD Courses"><span>Courses</span></a></li>
          <li><a href="//www.tcd.ie/research/" title="Find out more about our Research"><span>Research</span></a></li>
          <li><a href="//www.tcd.ie/services/" title="TCD Services"><span>Services</span></a></li>
          <li><a href="//www.tcd.ie/contacts/" title="Contact Details"><span>Contact</span></a></li>
          <li><a href="//www.tcd.ie/az/" title="TCD A-Z Listings"><span>A – Z</span></a></li>
          </ul>       
     </div>

   </div> <!-- End of core-header-->


      <div class="cc">
      <div class="cc-inner">
      <div class="h">
      <div class="h-inner">
        <h1><a href="/"><?php echo $title ?><span></span></a></h1>
        <p class="structure"><a href="//ems.tcd.ie">Faculty of Engineering, Mathematics and Science</a></p>
      </div>
      </div>


      <div class="m">
      <div class="m-inner">
      <div class="m-inner-2">


      <?php
      displayNavBar()
      ?>

      <div class="c ie-lte-6-force-word-wrap">
      <div class="c-inner">
      <div class="c-inner-2" id="main content">
<?php
}

/*
*     Function that inserts all of the standard HTML onto the page
*/
function insertStandardHTML($title)
{
  insertDoctypeDeclaration($title);
  insertStandardStyleSheets();
  insertStandardScripts();
  insertStandardBody($title);
}

/*
*     Function that inserts all of the standard HTML onto the page as well as the scripts and stylesheets used by the javascript calendar.
*/
function insertCalendarHTML($title)
{
  insertDoctypeDeclaration($title);
  insertStandardStyleSheets();
  insertCalendarStyleSheets();
  insertStandardScripts();
  insertCalendarScripts();
  insertStandardBody($title);
}

/*
*     Function that displays the standard page footer
*/
function displayFooter() {
	?>
      </div>
      </div>
      </div>
      </div>
      </div><!-- These are the ends of the class="m-inner"-->
      </div>
      </div>
      </div> <!-- End of the class="cc" -->
      
      

      <div class="core-footer">
      <address>Trinity College Dublin, College Green, Dublin 2.<br>Central Switchboard: +353 1 896 1000.</address>
      <ul>
      <li><a href="http://www.tcd.ie/accessibility/">Accessibility</a></li>
      <li><a href="http://www.tcd.ie/privacy/">Privacy</a></li>
      <li><a href="http://www.tcd.ie/disclaim/">Disclaimer</a></li>
      <li><a href="http://www.tcd.ie/contacts/">Contact</a></li>
      </ul>
      </div>

      </div> <!-- End of the class="wrap" -->
		</body>
		<!-- <div id="footer"></div> -->
	</html>
	<?php
}

/* 
*     Function that generates the HTML for date selection
*     Used by the javascript callendar to pass the date back to the server.
*/ 
function displayDateOptions($action)  {
    ?>
    <div id="div3_example" style="margin:10px 0 30px 0; border:dashed 1px red; width:205px; height:230px;"> </div>
    <form action=<?php echo $action ?> method="post" name="test">
      <div> 
        <input type="hidden" name="year" id="year" value=<?php echo date("Y") ?> />        <!--  Y gets the current Year -->
        <input type="hidden" name="month" id="month" value=<?php echo date("n") ?> />      <!--  n gets the current month -->
        <input type="hidden" name="day" id="day" value=<?php echo date("j") ?> />          <!--  j gets the date in the month -->
        <?php 
}

/*
*       Function that displays the hours of the day as a dropdown menu.
*/
function displayHours()  {
  ?>
  <label for="hour">Hour *</label>
  <select name="hour" id="hour" size="1">
    <?php 
    $htmlString = "";
    for($i = 0; $i < 24; $i++)
    {
      
      if($i < 10)
      {
        ?>
        <option value= <?php echo $i ?> > <?php echo ('0' . $i . ":00"); ?></option>
        <?php 
      }
      else
      { 
        ?>
        <option value= <?php echo $i ?> > <?php echo ($i . ":00"); ?></option>
        <?php
      }
      
    }
    ?>
  </select>
  <?php
}

/*
*     Function used to determine whether the values passed to the server are valid numbers and that the date is valid.
*     Used when th user is making a booking or if they are checking the bookings for a particular day.
*/
function isDateValid()
{
  return is_numeric($_POST["month"]) && is_numeric($_POST["day"]) && is_numeric($_POST["year"]) && checkdate($_POST["month"], $_POST["day"], $_POST["year"]);
}

/*
*     Function that returns a PDO object connected to the MySQL database.
*/
function getDatabaseConnection()
{
  try 
  {
    $databaseConnection = new PDO( DB_NAME, DB_USERNAME, DB_PASSWORD );                 //Defined in config.php
    $databaseConnection->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
  } 
  catch ( PDOException $e ) 
  {
    echo "Connection failed: " . $e->getMessage();
  }
  return $databaseConnection;
}


/*
*     Function that retrieves all bookings that are in the future from the begining of the current hour
*/
function getFutureBookings()  {
  $databaseConnection = getDatabaseConnection();
  $time = time() - (time() % 3600);         //Rounds down to the start of the hour
  $time = date("Y-m-d H:i:s", $time);
  $sql = "SELECT * FROM `bookings` WHERE `time` >= :time AND `user` = :user";
  try {
        $connection = $databaseConnection->prepare( $sql );
        $connection-> bindValue( ":time", $time, PDO::PARAM_STR );
        $connection-> bindValue( ":user", $_SESSION["user"]->getValue("username"), PDO::PARAM_STR );
        $connection-> execute();
        $queryResult = $connection->fetchAll();
        $databaseConnection = "";                       //closes connection
  }
  catch (PDOException $e) {
    $databaseConnection = "";            //closes connection
    echo "Error occured: " . $e->getMessage();        
    die ();
  }
  return $queryResult;
}


/*
*     Function that gets all of the bookings for the current day.
*/ 
function getTodaysBookings()  {
  $databaseConnection = getDatabaseConnection();
  $time = time() - (time() % 86400);         //Rounds down to the start of the day
  $time = date("Y-m-d H:i:s", $time);
  $Maxtime = time() - (time() % 86400) + 86400;         //Rounds down to the start of the day
  $Maxtime = date("Y-m-d H:i:s", $Maxtime);
  $sql = "SELECT * FROM `bookings` WHERE `time` >= :time AND `time` < :maxTime";
  try {
        $connection = $databaseConnection->prepare( $sql );
        $connection-> bindValue( ":time", $time, PDO::PARAM_STR );
        $connection-> bindValue( ":maxTime", $Maxtime, PDO::PARAM_STR );
        $connection-> execute();
        $queryResult = $connection->fetchAll();
        $databaseConnection = "";                       //closes connection
  }
  catch (PDOException $e) {
    $databaseConnection = "";            //closes connection
    echo "Error occured: " . $e->getMessage();        
    die ();
  }
  return $queryResult;
}

/*
*     Function that determines whether the current user is signed in.
*     If the user is not signed in, they are redirected to the login page.
*/
function checkedLoggedIn() {
	session_start();
	if ( !isLoggedIn() ) {
    	$_SESSION["user"] = "";
    	header( "Location: login.php" );
    exit;
  }	
}

/*
*      Function that checks to see a user is signed in.
*/
function isLoggedIn()
{
  return isset($_SESSION["user"]) && $_SESSION["user"] && $_SESSION["user"] = User::getUser( $_SESSION["user"]->getValue( "username" ) );
}

/*
*   Function that retrives a list of all the user profiles
*/
function getListOfProfiles()
{
  $databaseConnection = getDatabaseConnection();
  $sql = "SELECT * FROM `profile`";
  try {
        $connection = $databaseConnection->prepare( $sql );
        $connection-> execute();
        $queryResult = $connection->fetchAll();
        $databaseConnection = "";                       //closes connection
  }
  catch (PDOException $e) {
    $databaseConnection = "";            //closes connection
    echo "Error occured: " . $e->getMessage();        
    die ();
  }
  return $queryResult;
}

/*
*     Function that retrieves a list of all the course codes stored in the database.
*/
function getListOfCourseCodes()
{
  $databaseConnection = getDatabaseConnection();
  $sql = "SELECT DISTINCT `courseCode` FROM `users`";
  try {
        $connection = $databaseConnection->prepare( $sql );
        $connection-> execute();
        $queryResult = $connection->fetchAll();
        $databaseConnection = "";                       //closes connection
  }
  catch (PDOException $e) {
    $databaseConnection = "";            //closes connection
    echo "Error occured: " . $e->getMessage();        
    die ();
  }
  return $queryResult;
}

/*
*     Function that creates a new user group
*     The value passed in to the function is the name of the group being created.
*/
function createNewGroup($groupName)
{
  $databaseConnection = getDatabaseConnection();
  $sql = "INSERT INTO `bookingdata`.`groups` (`name`) VALUES (:name)";
  try {
        $connection = $databaseConnection->prepare( $sql );
        $connection-> bindValue( ":name", $groupName, PDO::PARAM_STR );
        $connection-> execute();
  }
  catch (PDOException $e) {
    $databaseConnection = "";            //closes connection
    echo "Error occured: " . $e->getMessage();        
    die ();
  }
  $databaseConnection = "";                       //closes connection
  $groupNumber = getGroupNumber($groupName);
  addUserToGroup($groupNumber, $_SESSION["user"]->getValue("username"), True);
}


/*
*     Function that retrives the group_id of a group
*     The variable passed to the function is the name of the group being searched.
*/
function getGroupNumber($groupName)
{
  $databaseConnection = getDatabaseConnection();
  $time = time() - (time() % 3600);         //Rounds down to the start of the hour
  $time = date("Y-m-d H:i:s", $time);
  $sql = "SELECT * FROM `bookingdata`.`groups` WHERE `name` >= :name";
  try {
        $connection = $databaseConnection->prepare( $sql );
        $connection-> bindValue( ":name", $groupName, PDO::PARAM_STR );
        $connection-> execute();
        $queryResult = $connection->fetch();
        $databaseConnection = "";                       //closes connection
  }
  catch (PDOException $e) {
    $databaseConnection = "";            //closes connection
    echo "Error occured: " . $e->getMessage();        
    die ();
  }
  return $queryResult["id"];
}

/*
*     Function that adds a user to a user group
*     The function receives the group id, the users username and a boolean value as to whether they are the owner of the group.
*/
function addUserToGroup($groupNumber, $user, $owner)
{
  $databaseConnection = getDatabaseConnection();
  $sql = "INSERT INTO `bookingdata`.`user_groups` (`user_id`, `group_id`, `owner`) VALUES (:username, :groupNumber, :owner)";
  try {
        $connection = $databaseConnection->prepare( $sql );
        $connection-> bindValue( ":username", $user, PDO::PARAM_STR );
        $connection-> bindValue( ":groupNumber", $groupNumber, PDO::PARAM_INT );
        $connection-> bindValue( ":owner", ($owner ? 1:0), PDO::PARAM_INT );
        $connection-> execute();
  }
  catch (PDOException $e) {
    $databaseConnection = "";            //closes connection
    echo "Error occured: " . $e->getMessage();        
    die ();
  }
}


/*
*     Function that returns all of the bookings for a particular date.
*     Function receives the date as a timestamp
*/
function getDayBookings( $date )  {
    $databaseConnection = getDatabaseConnection();
    $time = $date . " 00:00:00";
    $time2 = $date . " 23:59:00";
    $sql = "SELECT * FROM `bookings` WHERE `time` >= '".$time."' AND `time` < '".$time2."'";
    try {
          $connection = $databaseConnection->prepare( $sql );
          $connection-> execute();
          $queryResult = $connection->fetchAll();
          $databaseConnection = "";                       //closes connection
    }
    catch (PDOException $e) {
      $databaseConnection = "";            //closes connection
      echo "Error occured: " . $e->getMessage();        
      die ();
    }

    //$queryResult = mysql_num_rows($queryResult);   //number of rows not bookings.
    return count($queryResult);
}


function chooseColour($bookings_count) {
    if($bookings_count==0)
    {
      $imageColour = "#FFFFFF";
    }
    if($bookings_count>0&&$bookings_count<20)
    {
      $imageColour = "#FFFF66";
    }
    if($bookings_count>19&&$bookings_count<50)
    {
      $imageColour = "#FFFF00";
    }
    if($bookings_count>49&&$bookings_count<75)
    {
      $imageColour = "#FF9900";
    }
    if($bookings_count>=75)
    {
      $imageColour = "#FF3300";
    }
    return $imageColour;
}

/*
*   Function that determines whether the current user is an admin user.
*/
function isAdminAccount($username)
{
  $tempUser =User::getUser($username);
  if($tempUser)
  {
    return Profile::getProfile($tempUser->getValue("profile"))->isAdmin();
  }
  else
  {
    return false;                 // Doesn't exist
  }

}

/*
*     Function that checks whether the current user is an admin
*     If the user is not an admin, they are redirected to the profile page.
*/  
function checkAdminAccount()
{
  if(!isAdminAccount($_SESSION["user"]->getValue("username")))
  {
    header( "Location: profile.php" );
  }

}

/*
*   Function that checks whether the user has been blacklisted
*   If the user has been blacklisted, they are redirected to the profile page.
*/
function checkBlacklist()
{
  if(isUserBlacklisted($_SESSION["user"]->getValue("username")))
  {
    header( "Location: profile.php" );
  }

}

/*
*     Function that returns all the groups that a user is a member of.
*/
function getUsersGroups($username)
{
  $databaseConnection = getDatabaseConnection();
  $sql = "SELECT `groups`.`name`, `group_id` FROM `bookingdata`.`user_groups` INNER JOIN `groups` on `user_id` = :name AND `group_id` = `groups`.`id`";
  try {
        $connection = $databaseConnection->prepare( $sql );
        $connection-> bindValue( ":name", $username, PDO::PARAM_STR );
        $connection-> execute();
        $queryResult = $connection->fetchAll();
        $databaseConnection = "";                       //closes connection
  }
  catch (PDOException $e) {
    $databaseConnection = "";            //closes connection
    echo "Error occured: " . $e->getMessage();        
    die ();
  }
  return $queryResult;
}


/*
*     Function that determines whether a user is currently blacklisted.
*/
function isUserBlacklisted($username)
{
  $queryResult = getFutureBlacklistDates($username);
  if(count($queryResult) > 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/*
*       Function that gets the future blacklist dates for a passed in username
*/
function getFutureBlacklistDates($username)
{
  $current_time = time();
  $current_time = date("Y-m-d H:i:s", $current_time);
  $sql = "SELECT * FROM `blacklist` WHERE `username` = :username AND `end_time` > :current_time ORDER BY end_time DESC";
  $databaseConnection = getDatabaseConnection();
  try {
        $connection = $databaseConnection->prepare( $sql );
        $connection-> bindValue( ":username", $username, PDO::PARAM_STR );
        $connection-> bindValue( ":current_time", $current_time, PDO::PARAM_STR );
        $connection-> execute();
        $queryResult = $connection->fetchAll();
        $databaseConnection = "";                       //closes connection
        return $queryResult;
  }
  catch (PDOException $e) {
    $databaseConnection = "";            //closes connection
    echo "Error occured: " . $e->getMessage();        
    die ();
  }
}

/*
*       Function that gets a string representation of a users time remaining on the blacklist
*/
function blackListTimeRemaining($username)
{
  $returnString = "";
  $futureBlackList = getFutureBlacklistDates($username);
  $futureBlackList = $futureBlackList[0];
  $futureBlackList = $futureBlackList["end_time"];
  $futureBlackList = strtotime($futureBlackList);
  $current_time = time();
  $timeRemaining = $futureBlackList - $current_time;
  $returnString =  $returnString . floor($timeRemaining / DAY) . " days ";
  $timeRemaining = $timeRemaining % DAY;
  $returnString = $returnString . floor($timeRemaining / HOUR) . " hours ";
  $timeRemaining = $timeRemaining % HOUR;
  $returnString = $returnString . floor($timeRemaining / MIN) . " mins ";
  $timeRemaining = $timeRemaining % HOUR;
  return $returnString;

}

function DeleteBooking($user, $time, $room){ //This will delete a booking.
  $databaseConnection = getDatabaseConnection();
  try
  {
    $time = date("Y-m-d H:i:s", $time); 
    $query = "DELETE FROM `bookings` WHERE `time` = :time AND `user` = :user AND `room` = :room";
    echo "<h1>" . $time . "</h1>";
    $connection = $databaseConnection -> prepare( $query );
    $connection -> bindValue( ":time", $time, PDO::PARAM_STR );
    $connection -> bindValue( ":user", $user, PDO::PARAM_STR );
    $connection -> bindValue( ":room", $room, PDO::PARAM_STR );
    $connection -> execute();
    $databaseConnection = "";
  }
  catch ( PDOException $e ) 
  {
    $databaseConnection = "";
    echo "Connection failed: " . $e->getMessage();
  }
}

/*
*     Function that increments or decrements a page number. Used for adminaccount and displayeverybooking
*/
function nextPageNumber($pagenumber)
{
  if(isset($_POST["Next"]))
  {
    return $pagenumber + 1;
  }
  else
  {
    $pagenumber = $pagenumber - 1;
    return ($pagenumber < 0 ? 0 : $pagenumber);
  }
}
?>

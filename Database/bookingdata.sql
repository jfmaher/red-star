-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 29, 2013 at 01:43 PM
-- Server version: 5.5.25
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `bookingdata`
--

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE `areas` (
  `ID` int(11) NOT NULL,
  `no_rooms` int(11) NOT NULL,
  `details` varchar(128) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`ID`, `no_rooms`, `details`) VALUES
(0, 9, 'Test Area');

-- --------------------------------------------------------

--
-- Table structure for table `blacklist`
--

CREATE TABLE `blacklist` (
  `id` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL COMMENT 'The name of the blacklisted user',
  `start_time` datetime NOT NULL COMMENT 'Start time of the ban',
  `end_time` datetime NOT NULL COMMENT 'End time of the ban',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `blacklist`
--

INSERT INTO `blacklist` (`id`, `username`, `start_time`, `end_time`) VALUES
(1, 'BannerBob9', '2013-03-29 12:23:22', '2013-03-29 00:00:00'),
(2, 'BannerBob9', '2013-03-29 12:46:33', '2013-03-29 13:00:00'),
(3, 'BannerBob9', '2013-03-29 13:14:14', '2013-03-29 14:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `time` datetime NOT NULL,
  `user` varchar(64) NOT NULL DEFAULT '',
  `area` int(11) NOT NULL,
  `room` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `purpose` varchar(130) NOT NULL,
  `confirmed` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'confirmed boolean',
  `group_booking` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Is it a group booking?',
  PRIMARY KEY (`user`,`time`),
  UNIQUE KEY `time` (`time`,`room`),
  KEY `area` (`area`),
  KEY `room` (`room`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`time`, `user`, `area`, `room`, `group_id`, `purpose`, `confirmed`, `group_booking`) VALUES
('2013-03-27 11:00:00', 'AdminAccount', 0, 4, NULL, 'Testing Admin Booking', 0, 0),
('0000-00-00 00:00:00', 'BannerBob9', 0, 1, NULL, '', 0, 0),
('2013-03-07 00:00:00', 'BannerBob9', 0, 1, NULL, 'Blah', 0, 0),
('2013-03-07 01:00:00', 'BannerBob9', 0, 1, NULL, 'Blah', 0, 0),
('2013-03-15 00:00:00', 'BannerBob9', 0, 9, NULL, 'test pick room', 0, 0),
('2013-03-15 20:00:00', 'BannerBob9', 0, 4, NULL, 'Test Confirm Booking', 0, 0),
('2013-03-16 07:00:00', 'BannerBob9', 0, 8, NULL, 'Test Confirm Booking', 0, 0),
('2013-03-18 14:00:00', 'BannerBob9', 0, 3, NULL, 'blah test blah', 0, 0),
('2013-03-18 15:00:00', 'BannerBob9', 0, 1, NULL, 'Blah test blahh', 1, 0),
('2013-03-19 00:00:00', 'BannerBob9', 0, 3, NULL, 'testing blah', 0, 0),
('2013-03-19 14:00:00', 'BannerBob9', 0, 1, NULL, 'Testing confirmation', 0, 0),
('2013-03-19 15:00:00', 'BannerBob9', 0, 6, NULL, 'Testing blah', 1, 0),
('2013-03-22 12:00:00', 'BannerBob9', 0, 4, NULL, 'Test', 0, 0),
('2013-03-22 23:00:00', 'BannerBob9', 0, 4, NULL, 'Testing Cal for bugs', 1, 0),
('2013-03-23 00:00:00', 'BannerBob9', 0, 5, NULL, 'Testing cal', 0, 0),
('2013-03-23 18:00:00', 'BannerBob9', 0, 1, NULL, '', 0, 0),
('2013-03-23 19:00:00', 'BannerBob9', 0, 2, NULL, '', 0, 0),
('2013-03-23 20:00:00', 'BannerBob9', 0, 1, NULL, '', 0, 0),
('2013-03-24 01:00:00', 'BannerBob9', 0, 1, NULL, '', 0, 0),
('2013-03-23 19:00:00', 'BannerKate131', 0, 3, NULL, 'testing duration', 0, 0),
('2013-03-23 20:00:00', 'BannerKate131', 0, 3, NULL, 'testing duration', 1, 0),
('2013-03-24 01:00:00', 'BannerKate131', 0, 2, NULL, '', 1, 0),
('2013-03-24 15:00:00', 'BannerKate131', 0, 2, NULL, '', 0, 0),
('2013-03-24 16:00:00', 'BannerKate131', 0, 1, NULL, '', 0, 0),
('2013-03-24 21:00:00', 'BannerKate131', 0, 1, NULL, '', 0, 0),
('2013-03-26 20:00:00', 'BannerKate131', 0, 1, NULL, '', 1, 0),
('2013-03-26 22:00:00', 'BannerKate131', 0, 2, NULL, '', 1, 0),
('2013-03-26 23:00:00', 'BannerKate131', 0, 1, 4, '', 0, 1),
('2013-03-27 11:00:00', 'BannerKate131', 0, 3, NULL, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 0, 0),
('2013-03-27 12:00:00', 'BannerKate131', 0, 3, NULL, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `id_2` (`id`),
  UNIQUE KEY `id_3` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`) VALUES
(4, 'AnotherTest'),
(3, 'TestGroup');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_profile` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Decides whether the current profile has admin privileges',
  `Name` varchar(64) NOT NULL,
  `Description` varchar(140) NOT NULL COMMENT 'Brief description of profile',
  `Booking_limit` int(10) unsigned NOT NULL COMMENT 'Max number of days in advance that a booking can be made',
  `Max_number_bookings` int(10) unsigned NOT NULL COMMENT 'Max number of bookings that this user can make. Infinite if null',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Name` (`Name`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `admin_profile`, `Name`, `Description`, `Booking_limit`, `Max_number_bookings`) VALUES
(1, 0, 'Test Profile 1', 'Profile for testing', 7, 2),
(2, 1, 'AdminProfile', 'The administrators profile', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `number` int(11) NOT NULL,
  `area_number` int(11) NOT NULL,
  `capacity` int(11) NOT NULL,
  `monitor` tinyint(1) NOT NULL,
  `other` varchar(256) NOT NULL,
  PRIMARY KEY (`number`,`area_number`),
  KEY `area_number` (`area_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`number`, `area_number`, `capacity`, `monitor`, `other`) VALUES
(1, 0, 6, 1, 'test'),
(2, 0, 6, 1, 'test'),
(3, 0, 6, 1, 'test'),
(4, 0, 3, 0, 'test'),
(5, 0, 6, 1, 'test'),
(6, 0, 6, 1, 'test'),
(7, 0, 6, 1, 'test'),
(8, 0, 6, 1, 'test'),
(9, 0, 6, 1, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(64) NOT NULL,
  `name` varchar(70) NOT NULL,
  `profile` int(11) NOT NULL,
  `password` char(64) NOT NULL COMMENT 'SHA256 password',
  `courseCode` varchar(10) NOT NULL,
  `salt` char(32) NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `username` (`username`),
  KEY `username_2` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `name`, `profile`, `password`, `courseCode`, `salt`) VALUES
('AdminAccount', 'Admin Account', 2, 'dc1e452e0ddd36cf11826c8eb9b60e7d79884f74b3d902f7df51b91fc191e390', 'BA.Test', '-ÎðÚJ>³údMpµ+ß¶âÖÜ§ZŒý¤È›!¬'),
('BannerBob9', 'Bob Banner', 1, 'b37052dc1b3a251587b9255713d7b359f2ce936f7e921391fdd716a72eccccb7', 'BA.Test', 'çûæudò‹Ä\Z''á¦§UÃ}—M0ré‚¼îºŸÒÙ'),
('BannerKate131', 'Kate Banner', 1, '620fe259514a136d36b888f2ba7aeff086d8b27305275d60f1cfab75e4a59c1e', 'BA.Test', '  8CÞÿkàäe;Æv$\r/¡kíý’ ÆCš®Bâ"'),
('BannerKate148', 'Kate Banner', 1, 'b18cdbc2afb30826b763b961e807fc3a3789a494617b61bbd270c23356d3952f', 'BA.Test', 'Š<S©u†kùÃ5Ó“óÕû½vn¥©84·ˆÇÂá˜'),
('BannerKate67', 'Kate Banner', 1, '1559af4c64ace1b0f5a35d1128f238cdfc8f539bbaeb8bea033ef9c8e9878a88', 'BA.Test', 'Î1iºUð¨Š"Þ¢²t±!Z‹‹CS}á;~äÿœM'),
('BannerMary32', 'Mary Banner', 1, '1da75b22c1781ddbc2e8064920ecc5e73154f4f2b30ddb5073fbf203c710affd', 'BA.Test', 'ƒåéS‡ ˜ÝééYaÌçÃýhç0ÜÌ¢  WÕ'),
('BannerMary75', 'Mary Banner', 1, '32b4aee3b57723c3d501bbc100c7aba55250de08e8457153fbf4f7f82cb281d2', 'BA.Test', 'H”\06šÊ’\\BÂèb¸ÂùÆ5|~­š^ÙEØ4¨Ð'),
('BannerSam199', 'Sam Banner', 1, '78c0499bc9c6c864f82670508b01d9e24ceebac0765138c90c8646489b53088e', 'BA.Test', 'îšO3¿”8òã&¡_ŸSbFû´BÒË¸Å³l)x–æ'),
('BannerSandra179', 'Sandra Banner', 1, '54523b9149494326a9c5693aefe074d74c15c727a3237d987bb4d613e305d54f', 'BA.Test', 'ú®Å¢p‰JŒÿü«qÖ‰,¿5õ˜ö]åð5efâ>Á'),
('BannerSandra61', 'Sandra Banner', 1, '5915c0bea107ee52f8794dabdc3af8a3a70b3f5350f68ddf36f48162ca06dfe9', 'BA.Test', 'âÅ\r’-mœ}Ö8(˜${åŒûöˆîË¼ï3ý''[Èè'),
('BannerTom97', 'Tom Banner', 1, 'd0f4b2dff0ea1ff6f33e9ac3eb8b094bf01bdd88034fc60802bd24286b98bbad', 'BA.Test', 'Úø‘@DŠgvGwÌÕ"˜óaÒN8-Pª¡k«¨'),
('BlackJudas41', 'Judas Black', 1, 'f2c46ad6816325786d3298ea467e231a2b34abf37d64890192f4daac439720c5', 'BA.Test', 'Ãâ.#Ln\0Ú=\Zø‚×b×Ì^>uHéÔ¢mášŒ§'),
('BlackKate64', 'Kate Black', 1, '1aefee717ff85bcc50db626b26807743c6d37ab80ebebe285063cea91ff947ae', 'BA.Test', ',Û•YŠˆ*\ZgX.ú-Èæxá“etœ‚ex6*z¸'),
('BlackKate81', 'Kate Black', 1, '28ef51e683dec44d445732329457883dbbec9217811666cd15224d8b01c92212', 'BA.Test', 'm8RtåÎO¡ÞIîÕ*''XýÀRMºòï”Ú<^‚‘'),
('BlackMary116', 'Mary Black', 1, '621b05e3692ff4bde26c01e138ccbffb1dbb82c4800b3425cfd5b429cec32821', 'BA.Test', '™&mªâÀCåGÕ½''\0Íàº¨È¯ûÈ‹ažëžTøÛS'),
('BlackMary26', 'Mary Black', 1, '015d9c5bb8410c0d7a5dc458a04b79ce4cf0d3e0101ecb62146da7a144811f6f', 'BA.Test', 'Ì«aXü¯Tç„ÅœŠÔ"D%<=—QÓnÿg%é¹'),
('BlackMiranda192', 'Miranda Black', 1, '5fc8bf134c81dbdd1f55810138e6229850ebe12e574b246b3f96dcace36c86db', 'BA.Test', '¿þ,.ÞÛó&Ï<¿L(mÐ®S=,¶W÷Í~e†N­8ø'),
('BlackPaul153', 'Paul Black', 1, '87257f9adc6b17c027ada8f29f4769aeaa31005862828e5961d5471e9b29184f', 'BA.Test', '×\0-n0 ›]Z=eˆ''ÏK¨¶sïªa³>¿»V”vèoL'),
('BlackPaul34', 'Paul Black', 1, '9871bacd6a9c41537c9f62f93955c7da43588f869177016905a96803ac8871b2', 'BA.Test', '™P‚þ1f’-à^. õ÷òzNê¡òHÓ'),
('BlackSam124', 'Sam Black', 1, '078f6ef7df8da5370501e45927386e08315e75be94417141bf5e988eb9949051', 'BA.Test', '[„¿.Š$ôx#‹vè”gºeÌm;~î3Ÿ»F¯Ì'),
('BlackSam167', 'Sam Black', 1, '13180fe5adcdcee8427a492ce5ca7f3b5def67620e18964d0b61b3bd49ce3e8a', 'BA.Test', '±JrŽ£¹*Ocù(lÙý0\0n§·ÜØG¿p¢Æë?³'),
('ChurchillAmy37', 'Amy Churchill', 1, '528b39f7c097967b1d72147ad98c957c7283c4162e29ff39c24856723d09170a', 'BA.Test', '\\˜''°+nQã@BY@²^ê>$}Of>7Û™OÄÿ'),
('ChurchillCormac89', 'Cormac Churchill', 1, '0865f4cc82fc420b47feee7c5ad468d5b0541e324e0eed8af4c405b427048bed', 'BA.Test', '^¿ú¨{ÂôedNv"1×†à  «vèÁÐÍ<Ó)jó'),
('ChurchillFiona95', 'Fiona Churchill', 1, '333aa302199e699aa1f26d428229155d764160e60774a21b7a2040e2d28799b2', 'BA.Test', '¢—Âž˜ÐÛpÁ?=Íl€™|Ó\Z>—G­zÄ°æüb'),
('ChurchillSam31', 'Sam Churchill', 1, 'a7e058ef6c972013dc61493e55e228d552e283184cca10d9039b7f8ef5949c17', 'BA.Test', 'ˆÍ>zU§jÖ?f@²0Ú¯û*F''¨pØžŽIßžË¤¨'),
('ChurchillSandra194', 'Sandra Churchill', 1, '96c86c31a111c1ab3124700a276755e9bfed42056efa4a0b1004195eb6ff29c6', 'BA.Test', 'y°pL÷H©ï,[—´ MJïÿ7ÒzU²Ë”ÌeÀk‰q¼'),
('ChurchillSandra71', 'Sandra Churchill', 1, 'd625038a0d9bb0aaf71eb460e582c8fccb25220ea1ed4a0c4eaa4fe071942055', 'BA.Test', 'fÿ¶g5|bk¤ÏJ¥úåâB-!b¯0^ŒÿNÍÍ'),
('ChurchillZeus146', 'Zeus Churchill', 1, '5c9ddbd28de61a471d6ddef10ac8877ac5e7afdbb33f14475e05ed36625a1c41', 'BA.Test', '£°ìÐOÚ®¯\ZÏÎ5ŠÔ-´ž>u÷Ph=w>'),
('CosgraveBob177', 'Bob Cosgrave', 1, '7afd5dda29736e3e4de60e800c060766769edea7f0010edb63de64a2790a758f', 'BA.Test', '·Lüál®‹¼@+c–Ç_9ê\0›`ý$,8‘äÏš–L'),
('CosgraveCormac174', 'Cormac Cosgrave', 1, '929f794294b5483a0ead6fa0e27eca75f10229241a341f1e8a12402248ddfef6', 'BA.Test', 'Ñ]¥ñ9ÂÏ½ª»[O³\ZÓ?Žr 3m -\0“A6…¤'),
('CosgraveJack108', 'Jack Cosgrave', 1, '52339bb9003d40f261a36f6032bd9559129e015d7a2b9fc774f4ca28716f10f7', 'BA.Test', '$Aÿ\n 1Z£!BErr™}ýUï|Û´ƒsŒ4#Š¹'),
('CosgraveJack168', 'Jack Cosgrave', 1, 'c320d3991a9a103b842147adbbb85d745d08b875129d229822e52aa934518625', 'BA.Test', 'I{ŸôÜ9•8oŠ‹ð]tü\0XÙ‚…Û)¯¨â/f:ÿ…*'),
('CosgraveJudas96', 'Judas Cosgrave', 1, '9e904deabf12938dc66aef7cc88a4ba06328f1b89a1c2225e8b3378612bc1176', 'BA.Test', 'NL%ƒ®Áå†c-îb¤K…/caj)\0ûÔûg¦cX'),
('CosgraveKate51', 'Kate Cosgrave', 1, 'fe9a15f190caa4a031c95d597406cfa25a78d2952e4506f82a4381d2fa551b71', 'BA.Test', 'w\0„™ì’¡^Ð#Žð+w¤«GlWÂòË¬Òj¾ö'),
('CosgraveMary150', 'Mary Cosgrave', 1, '6ab69b5c0d1c0365949390f4386018d19e72dd01b853774949846e3617e2b256', 'BA.Test', '4_sÚe“†Ð0pÓ¨ÈÜCÔ»T´ Ïä«<|GÑ#'),
('CosgraveMary39', 'Mary Cosgrave', 1, '35dcd445ee0e5878fe1ee29062bceb67c2835d0fbf07b434afc887ebd7060d72', 'BA.Test', '5Œi³}m•àA&ƒ¥w£ä‡ê1[P8''\rÝS’I'),
('CosgraveMiranda175', 'Miranda Cosgrave', 1, 'b8c8f3c26efa0cab8562d9e09c901ad3fdafb4daff77ab103e6db377afb36c2b', 'BA.Test', 'zàFæ—ì×»#ÂxboÊ°yôìw2T*ì'),
('CosgraveMiranda72', 'Miranda Cosgrave', 1, '3a4c343f8e34ee3919666948e137cc161cdfacef59915273efcaae8a463a967e', 'BA.Test', '~Sšy¤ùÃ°@;–Ÿ¿b1J@‚£ûË|UKW¸Ñ·'),
('CosgraveRichard163', 'Richard Cosgrave', 1, '9d47381864943a8da8a084709f129e89833dd46f24f8868c94a1070a05c1b26a', 'BA.Test', 'i2ŠÍâ8TÍ0„îŠT@³N—¶-íÜÀ(ôç¼Ù{'),
('CosgraveTim127', 'Tim Cosgrave', 1, 'c44630472a43b30284d36d17537f6b8b9fa441222892453865a357e4d113f1fd', 'BA.Test', '„x<ë,B¯„Nÿ³µ-ƒËÿÿË«Šõú\Z»/ô'''),
('CosgraveTim128', 'Tim Cosgrave', 1, 'ad4e0a67aa2b6bbe293abbc9a4da16fa803b60e85cad62e3625b75d3d84f2cbd', 'BA.Test', '¥…YUFÅ0Óé[•‘ÏL¹^±ò/\nTPìÂAžþÒ'),
('CosgraveTim42', 'Tim Cosgrave', 1, '321a3d326d1b6036f74951c172eead0f0fd235935edb09898ca40be3362aca08', 'BA.Test', 'Î°…/¬nÊŽ¼þêN¸–ZÕœÍk\0NZçÀÊb™¨¯a'),
('CosgraveTim73', 'Tim Cosgrave', 1, '2ecbe7deceb4cbf8f8c3743a6c7ebb7a957792b588c57879115307d8514fd477', 'BA.Test', '¤¬Striˆê|sËJíùÒI¹®]6²ŠJWù¦m0‘'),
('CosgraveTom112', 'Tom Cosgrave', 1, '46ca01045bf4b2fb9d940a17f922a574472b74ee308523b28293bdb1ebba49ce', 'BA.Test', '¼oÞâátžA½˜°CÒe-#8´  9è½ª_H'),
('CosgraveTom197', 'Tom Cosgrave', 1, '41e0b74d4e7682b2ce35c05c065f436e9b14ff63c8dd4a0dfe72fc3bef9c2b78', 'BA.Test', 'ú¬BGßºep½E6L§(Â\0™¼´Î6´­ßÝ<í'),
('CosgraveZeus70', 'Zeus Cosgrave', 1, '2cfd5a0424d4a083c5951a4c0742626fabc6139e9b863877d03f0ee79be8a7da', 'BA.Test', '°.#“3ú/:,îÊ\rs  s®xÁaµ…þr¶ð‡‚ªÅ'),
('FrenchAmy92', 'Amy French', 1, 'd230e2572fdacc45d0bd5b5b3ec3c9fabdc5a708b0d565c303d919e734d3598d', 'BA.Test', 'tÎ''Q\r÷ŒéŠ7êªñ£ÐhìÿW¿¼È¾Ìy©WÿY'),
('FrenchEmma114', 'Emma French', 1, 'e221b27880ad3c6ac040081d991ee65d04c8941ed01debb8a159d14138b1f890', 'BA.Test', 'Hê±¦§©j,)áN´Žy{°ÙÜZƒ¨hŽÌ-l­'),
('FrenchEmma54', 'Emma French', 1, '555a73d73ee7d81761c98c443de3694a279af50dd0df108d221ee5ddc9be1ffc', 'BA.Test', 'ô8:ªøi\0c¡J5A=c7ê»À-\Z‹maF^Õ#'),
('FrenchEmma8', 'Emma French', 1, 'c52d373fb62ddabdf29e75fab8dfc72a9ceddb5390a11236052beb5dc55cd0e8', 'BA.Test', '~wDmUÉ€(g¾ÌFÔgh³]»‹ÅÅÌ€)µ\\Ó'),
('FrenchFiona16', 'Fiona French', 1, '8637872b1f91589f155a9122f08fed0daf237137ccf2cdc1f547cd4220ab4b8b', 'BA.Test', 'Æ½?,¬ï ³ÈÇ\0ú…Ôº…4X–X”Ý\\$ÖÂyHt'),
('FrenchFiona166', 'Fiona French', 1, 'a0d24cd25e49dcc3d2d12c11d643fe6ec24ecc1fcb0a937d628e958638beec8f', 'BA.Test', 'WR‹­ÿÇÜyš€/º¾Š3WYð''T=BòÙG>œG'),
('FrenchFiona52', 'Fiona French', 1, '51c75b7e1a89e7199a49c013e072d62ddd6b314c972be679f06232708ed03f91', 'BA.Test', '‡Ý2¯Þä5è5¨^  Ã€óiw"‘Õ¿4ç7$¬O³'),
('FrenchGlenn104', 'Glenn French', 1, '117e117ac34d397a7927462561c21fc1bb5d10cce50b3a8ade4b0648264feef3', 'BA.Test', '<\\AoÝ—rˆ‰÷DïGe¶±‹›ì£·:¥ä³,±¨'),
('FrenchGlenn15', 'Glenn French', 1, 'b45494a5f77d601965a040ab2b558536597dbc5d97733cdd8c919677c222f624', 'BA.Test', 'ªé ùÎ¸}\n·œŠ «Èƒ8Ñª. Äz\rt¿<W'),
('FrenchGlenn152', 'Glenn French', 1, 'e2b53370616f20f7cfcdffa762ae4f396d3a911d488ba931f196bf72551f2d57', 'BA.Test', '!Žÿ>¶a‹¦ºe¥òj~Z¯hÖš ¸Ó¸Û–¸(®×'),
('FrenchKate3', 'Kate French', 1, '320f23b78d040a99f7e2bba950263df831933cecbc389af058ef90361010ddff', 'BA.Test', 'J©‹s~¤³I@ÈP¡²QóöNì\rçÏöƒì'),
('FrenchMary109', 'Mary French', 1, '167afda4f5cc2a52a44b93414c0d42e3791274565bf592487cb3df64bb9c6bbc', 'BA.Test', 'ÛíWþT-ª§3Ç*˜Zå04ã²\\­DðQ’'),
('FrenchMary178', 'Mary French', 1, '92e3b7db9e1ad5be7cfc5eea8d31317c876834c413401d72d933b267cf0e6911', 'BA.Test', '\\§I»£Z©*½X»7»ëÖ¡>H¸h:ei"jlÂ–'),
('FrenchMiranda136', 'Miranda French', 1, 'd7ad588f1ba3af6d76e20f96d8c82bd311d20217e4ab42a33ac3f664ad0f636f', 'BA.Test', 'àªbxÜH  \Zæ¯ÆkCèDÔ°­Í/M@ò$Õ)jž'),
('FrenchPaul115', 'Paul French', 1, '1112f3134be2650b3330faef8116ec80ec5da7d8e49f3c2e53eeaabcb8f208ef', 'BA.Test', 'Üìý[Onš«³7,‚‹°Žþ  ¢À¼+C=¥ÌÖ|'),
('FrenchPaul29', 'Paul French', 1, '1072335975c9d45794eb1276148e4bf6ca40ba83d3047d034ce4357b4f9a5fed', 'BA.Test', '¯ÄØL©ÊEÙnÜVöD°ÓäU„ð}‹Ecï[z·5¹'),
('FrenchSam11', 'Sam French', 1, 'b8d6f45dbb83cde2be2d02257cfe40b5ed1ff962fc98bbf502012e316262d4fa', 'BA.Test', '¸‚Ñ¼\\h­Züœ.e)8š¨ˆàsI« ¼ü·Mª²$'),
('FrenchSam161', 'Sam French', 1, '72740c8e9a9c43135d03a50c389d38d10d1982bfdb9b03c87590862d52ff913c', 'BA.Test', 'ÑšR3{!DAL³®máOyËøF-M2f›Æb\r»ˆ'),
('FrenchSam185', 'Sam French', 1, '69215b11f0a31ade4e1e15442bbfe6a422ee4165cbf0dbfb61f5b1b4599adf94', 'BA.Test', 'Ÿ–/B=yd¹JCœ§Æ‡ÞIÝÛúúT¯±¯Qž)•'),
('FrenchSam196', 'Sam French', 1, 'cbe20703bdd5f55549a36b93387496dd3206662c108c830cfe18baa7335ec0ec', 'BA.Test', 'O®õ§Ë`†t_U@`$!K·DT¶jøj†ÄÇ—'),
('FrenchTom151', 'Tom French', 1, 'bceef181dfdea6afbe8376c92d6eb41d5f330b22aeafaa3e9fa6e084989238fa', 'BA.Test', 'rgWö!VC–¶¶îüt•z†ê„vý"PnÓ3=–ýA'),
('FrenchTom160', 'Tom French', 1, 'e0860c0c97a21a5b8dcf889556e6b5516d9f2fdb9c04edb000fad92d37734142', 'BA.Test', 'X«u2É·zöž,•†%ÉÆø~}Ú#¦•"$?Œ‡L™'),
('FrenchZeus23', 'Zeus French', 1, '3588e98a0b2673272a8e65f30cdbe55fc87c40e0613fa6bf559831a57c0a87e1', 'BA.Test', 'ÉM`­=qå6Ú ÌQ:Î&F˜>…KŒ¢HHül'),
('GleesonFiona123', 'Fiona Gleeson', 1, 'a1d26ae2e8fabf5cb4f9340196ff3eb4eebd3819a1af1a49d5ba5c102a5c0698', 'BA.Test', 'úê¡`kW}“¸UyÊ×ü}&Šª‡&¼—<Ù9‡³ß¹'),
('GleesonFiona13', 'Fiona Gleeson', 1, '9a57004a2374eec626414615364ed9f10c7a053205488e7f3b1f4809a9b9ce88', 'BA.Test', '¥€ô£C§sÔñ);]xã8aø’v?s8j9J{†²'),
('GleesonJack141', 'Jack Gleeson', 1, 'ecfe20f98026107384dde4f7a355c21c95fc4f58e3112bfdbd92ad5d4fc01c6b', 'BA.Test', 't‚Öôæ¶¼1\Z½íÛK¼‚³5Ê·lîÕž%KÀV< '),
('GleesonMary162', 'Mary Gleeson', 1, '9d47acdce29da17907afaee0d6e984f4d4a1380759cebce1fce61ec8747813d9', 'BA.Test', '!JŒ­Ìì¼\nÜxm&ióÿ&òþ:o&ò–ìÚ;-w'),
('GleesonPaul40', 'Paul Gleeson', 1, 'cd524e205681e1bfd8d88e026a5520c192993e00ddef7ca0340b8a931da52151', 'BA.Test', 'UoË‡o&<q`÷³ÄýBš$mŸ!ÐÓ²¢Î1b5'),
('GleesonRichard30', 'Richard Gleeson', 1, 'b409b728867f547168a314ecd15438a370da7bf01f826712099125e3678a3daa', 'BA.Test', 'É<ë)ÿýþbˆ.„Ó8GÙ—NuÀY °áË:®fµ›'),
('GleesonTom63', 'Tom Gleeson', 1, 'a9dd3fa5a671697d7b26d5ff7379d57fe42cf32e571e26bce539f38a542a81b6', 'BA.Test', '¦I²¹¼‹Äa%ì\0p@æ—ô-ÿPœÝR\Z£'),
('GreenCormac121', 'Cormac Green', 1, 'c3b6eeff98fda72e20a60efc9730e7213ce0a1fc34d2f61d3021d6a45e61de2a', 'BA.Test', 'ågïŸµÒÿ[3µÛwìt6V¼eŠÕv\Z¾cFk±g'),
('GreenFiona122', 'Fiona Green', 1, 'd93e3f94944b61c9e7070361a9ba2f07357d3e0213efe5f734280083450949db', 'BA.Test', 'ìÞñ˜_•p@/ 9 ''aèü|ZÎlÁÙ­a¸L`'),
('GreenGlenn101', 'Glenn Green', 1, 'fe6f6337c3ecad9a30f531243271bdf29ec11b60f79a9824fb2ea3363dd02d7e', 'BA.Test', '‰HY¶7•ƒ4ÈŒ4¥Ø1zŒz/8\nÛï%| á¥Ñ×¸'),
('GreenJudas36', 'Judas Green', 1, '67817ab08d303e7b6efdd739c58761ccf2a8b50800021ec7e9d08504c2a612ac', 'BA.Test', '-TÍš\0Çý;_W‘auÜ˜/åHlÝ§¯{&Ã'),
('GreenMary120', 'Mary Green', 1, '74b1bc7cc5158bfc5a43e0951c2a5bbfdc7055dcb65e267b4bf86b0e9507558d', 'BA.Test', '\ZA]Òˆ1Gfo³¯ªý2]Ñ§@_úG  v:õÈ'),
('GreenMiranda176', 'Miranda Green', 1, 'bb9168d49f6f026f688490b8c34fdad7ea22c0a85633594981380c299a27e96c', 'BA.Test', '´ïA×]‰¡¿ì¡BwË9šÒ£s¯Ëž^,F+ÑÊgÜ$I'),
('GreenPaul171', 'Paul Green', 1, '49f70a4755382f1f1be4f7468fb1ee32dea0fcb8ca207d880c4a6cff1b4ed58d', 'BA.Test', 'ö&‘ƒ“Zq)Äð@ðQ”‘JÕ2U˜Bë†ï¨–<\0`'),
('GreenRichard100', 'Richard Green', 1, '496bc95de8a572980ac414a9b6ec8985a7a15f8726418a1715a3e8ae4951b47e', 'BA.Test', 'h3ÆÏË¦¢ýÇµú›%¬‹XÕ¹ùc#òJf=«b'),
('GreenRichard5', 'Richard Green', 1, '6aff1e7fe67674025df16b0bc2187fce1cdf65e7af58a21e1626f998bad1f7a5', 'BA.Test', '½¦Ý.VT*''²§1è¿2ƒûO_\rƒ9H1)ÅŒ6î'),
('GreenSam149', 'Sam Green', 1, 'ba7548f3a7c560c75eff9f7015b9d6a5b5d4cfa13a0fb1c940d955b499e0bbab', 'BA.Test', 'EA3˜V¦ûÅ{VkÚÊ©NPÿ¡y{´² ÒÀd×Æ'),
('GreenSam80', 'Sam Green', 1, '7b10ac70650125172f39977c7fb5f5d7ca37fcb0ed6a74b84fc1449ecb6ed5fa', 'BA.Test', 'uÛýh7&¯ˆú''ÿ©NêQ¦ðÖ\rÔ''ìr¹Y›'),
('GreenTom165', 'Tom Green', 1, '61e0d911298eb26eabdda39ab83250dfe9d4ab770939b7c37b23df455dd8fa69', 'BA.Test', 'Y%wÎ,›¹_`aá˜£  …¦‘8ËyÚ&l>W?üK!”'),
('HughesAmy154', 'Amy Hughes', 1, '53e156065cb4cf6ceddc6d84b76f80b482127f305590915fb1726ad8d9029973', 'BA.Test', '*÷Ô%Œ  ã…}2VWEjèFƒ¥:2Ú„\09c¤'),
('HughesAmy84', 'Amy Hughes', 1, 'eb032401e904bfb9774fe370e65a58982e9a008d0a778a464a5d2114a057695e', 'BA.Test', 'ÕeqZUcQ°C.ò4W…Ì€Æ÷ýý™xÎ½Ú'),
('HughesCormac198', 'Cormac Hughes', 1, '2cc98509b67eefe9aedb6a81a0971ee0aec01a928ae3b2a253ee90228fb3fbd3', 'BA.Test', '·:(\r§óŸ''æ6ŠL„ÁÁª«½>[:;És—L\núy'),
('HughesFiona25', 'Fiona Hughes', 1, '9b860220e7fa86dd696a45a7d9b0753c2f92f20e81d4a171b6283f20efeac004', 'BA.Test', '¢‰žd_å”GösM#‰Ö‹_L‡¯gÂþ3`Î'),
('HughesJack46', 'Jack Hughes', 1, 'd2091523b55d43954cc4d51705489eb9656821d37423b7bc88f1156b52cf8e04', 'BA.Test', ';âUÁ8lÉVÆ­$52Ó™xé…Gmc#“Ë^3ñ§'),
('HughesJudas129', 'Judas Hughes', 1, 'ea3fb1165e9e287094907d73031f2ee0b47ca5e109d942375537a2ef8975f739', 'BA.Test', 'úí²‘ï]|¶G™½€¢QWf\\ã jqØï‹#s‹È™'),
('HughesJudas172', 'Judas Hughes', 1, 'd7f4e886000dcaa3a4c8164bb2ea5e4b89aca69cb6eea9bdf95e33443631cba1', 'BA.Test', 'ÍÔÜ/øe—ªƒ$@"¨ÅIo{ÿ‹aA›²÷§ôÆ%Â'),
('HughesJudas91', 'Judas Hughes', 1, '19583c251bd8017448ce860aebb23091c65ed09cd74a25f323be1b3a039dc633', 'BA.Test', 'Ô.pwYJ°Ýç‘Ã™áÛfòÍ¥¦š:y›JÁïõ>p<Å'),
('HughesMary110', 'Mary Hughes', 1, '9d905d5b47427f0464c61a056b1a7e8712f3944ec6ebef931f76d9e76c0597e5', 'BA.Test', 'ÂgWï›âÿØ‹sµÄëý2à7¨-ð1Z±´sKÒ“æ™'),
('HughesPaul45', 'Paul Hughes', 1, '5648fa7504ca4532ad81ef7f6f7e9af3d1f247de998785778b7950d5f9214d61', 'BA.Test', '1òã÷¢¸”È9“¡·¨lW5PZ| F!©Í²ÁP'),
('HughesRichard117', 'Richard Hughes', 1, '44a5e591a118d5e6e1befdc30373d4339cb96d2e732d9ca43433993bb12608fa', 'BA.Test', 'ÑîÉK3„jîB`×:»X\Zj+ðòàÜrªõ¦bC‡”º'),
('HughesRichard158', 'Richard Hughes', 1, 'c2ecf273104b90b5a9a0908f38a246d4122742a126fef652518255d233ce905e', 'BA.Test', 'KîôJ¾Y}±œ–ŒÌC/†cNc‹Ø `ˆ¹@»U'),
('HughesRichard33', 'Richard Hughes', 1, 'a56aaac85e5ddaea5f4fe5200d4d5db0d3d8529ac661abc7dcb8851eb807151e', 'BA.Test', '×ÛBÍ?ÖMU+³qò²jáV×„g5¶ëÒ#°L€'),
('HughesTim193', 'Tim Hughes', 1, '90cf414d11aef4b81e5db7a84e56df558874fb1928e19eeb55e3cd6b4df2b971', 'BA.Test', 'ŽgxX¡9¤ûé|§éza ’Åwã¯£|þq’Õ'),
('HughesTom189', 'Tom Hughes', 1, '2e0cc9cddcc8c613cad8d4471d01e152c8c6b0eb1c03d3bde9e8d26bcb9aaa12', 'BA.Test', 'ÀÜ²p.AÑûBab‰ÁúV¢wmóË>ô"2c\nÿQf'),
('JonesAmy126', 'Amy Jones', 1, '7eec53ceaa85b946c374168ddcf982ea27c39e6f0a1b175dfd3e3faedb2c8315', 'BA.Test', '¼ç‹YÃ=tf¦‹Ó¥¦šC>°@l‰\n¦çOxÝC+'),
('JonesBob48', 'Bob Jones', 1, '7c248f670ee7638d4879304f140c152622556b430633661ae98b4ebadab9d9ac', 'BA.Test', 'YÖ4ÓÞˆ’m ®¿qÈœ*¸a³Câ›”+PËpÈ'),
('JonesCormac57', 'Cormac Jones', 1, 'f1746590c953ac510d602ef0759bce43fdc70f9501a900d04fb075f3e3e3980c', 'BA.Test', 'Ö|ƒžás”¯CÐ<ZÛo;š:K¼‚‡"M&ï‡Öô5'),
('JonesCormac77', 'Cormac Jones', 1, '19346bc78c0469e2af716d045b9b26aa927413d03d9e14f86ebfdc4393c9df03', 'BA.Test', 'Œ˜§¢¬-3Ô$Y=®Ý.h#8/Xvi+•ÊÊóX+ù'),
('JonesEmma94', 'Emma Jones', 1, '2f8cc6b21dde9a2507a161c490f22b4f3c4528188f9ee7742fc863f02c8332db', 'BA.Test', '3Îp…†"qHü¸õÅ•ßoabÝB‘¿TDÿF(oF¿'),
('JonesJudas118', 'Judas Jones', 1, 'b020248b0d50d2085f787a97cb6ae69381c9bd8ff9f3ad7c37c3ac033ee05c29', 'BA.Test', '–(¬J"²A¨Úøà\n»ä¢o!vhˆžÁW‰÷Y…ö'),
('JonesMiranda12', 'Miranda Jones', 1, 'dc6796fafadbcfa5e18622d4d27c8dd34e3dfdf4e383181f0e222f19ad6efb35', 'BA.Test', 'ü¤\ZB8‚9é>’·&›B”ÏÃÎ''3õŸH uç'),
('JonesRichard186', 'Richard Jones', 1, '7a31fb74edcc442cb719931131eeac8c389c8b818a9283a11243869acb866ea1', 'BA.Test', '+6z•‰škw!È““8|Ò®]QB8-ëÇÎ¢(+'),
('JonesSam140', 'Sam Jones', 1, '5ca011294edf79366d81f7ba629a063bc47945b708d8719b09a1cea8bbf70a7c', 'BA.Test', 'h9â¼1¦ª“n‰5pJK·(iÁ­èÍEöÓ³_(ºrž'),
('JonesTim53', 'Tim Jones', 1, '2b4cd2ae7d99c0271df69b02b5636c8da4d69b9084354a833f045a62d3730125', 'BA.Test', ':xÆˆË™xM…=øvÁh¸lå’#ÝáTfR#ÉJ'),
('JonesTim87', 'Tim Jones', 1, '9e3c0c98711024c697f165b6d1ab454dcdd685b241f983e4b95a3f676c3471d5', 'BA.Test', 'KÐ¿7€‘3¡J÷U²¢È]ËÝ#ÖðÅiZ<Úo^'),
('JonesTom142', 'Tom Jones', 1, 'b61fc4da8c13d84f674ca9f0d281ce873b46eade0808e48ec721e68821433ac8', 'BA.Test', 'E…Q@T¨ã‘ü¦Y''£À‘0µû?e8®þœlò¹\0—'),
('JonesZeus156', 'Zeus Jones', 1, 'af91fa02005cdd3c1d59692e7ef32b8bda1d1f469d114fbcae8fad57097974e8', 'BA.Test', ':éùÇODÝW_$VàÊY.ÂýEÆÁcdÑò/N'''),
('LeninAmy56', 'Amy Lenin', 1, 'fb486bc8604754a3360e2def4d0c4d5f3ab8555b4e1991b9e15d486c9c1e7e30', 'BA.Test', 'LìÔ0öÐ¦kµ4Í/BI?÷1F§ð‹Wß›ðMõ¿'),
('LeninEmma130', 'Emma Lenin', 1, '5b7eefb364bbac64d007e8debec863f7a2084519be9c8744ca736525451c278a', 'BA.Test', 'Èn÷HÃ\n ¶ûWtƒí´îæ’”Øô-_Óß2A¤—'),
('LeninEmma170', 'Emma Lenin', 1, '8407c5304e1137de2572cad5f6ba5dd2e59f35de5d54f6f86abd0436f7b4effc', 'BA.Test', '%hûÉ6K©;™²Ì‚yŽÔä\0š½ù~ª·üWØQ”'),
('LeninEmma7', 'Emma Lenin', 1, 'e91b095eee90306fa346c30ae04bbf135cbfe68189495b7ab22ce14eb221fd7d', 'BA.Test', '2ÂX%x.4‰Í[ÿ{¸òïxâ«\r\nó3NÍÿµÕ‚Ó'),
('LeninJack164', 'Jack Lenin', 1, '12f7a708469098a7c994eb05a13fcf912d13f4b779cd6243b218d8b5b96f52b9', 'BA.Test', 'ãá”îâeÙLUé¨\nMV«nOÛe­™@%Câ¢‘'),
('LeninJudas125', 'Judas Lenin', 1, '26b88ed319349798d8493bfb4285d0d897cc520a564bae04244b0521ae548d50', 'BA.Test', 'GÒ^\rGÝ`Œ„5Ä\\5XûÓ¾+rXÈúVgš@\\Ø:\\'),
('LeninJudas169', 'Judas Lenin', 1, '96fae93e6ae2fb9813db06a76405bd4036436a23a618333a89641e79aef90a6c', 'BA.Test', 'ýå]ƒwµ½óúê\\9õ*/x6\rðÁä»Á¡uÙ©H,'),
('LeninJudas68', 'Judas Lenin', 1, '103626b4f4b6605335f6d429512cd91189a67642fd67df6d8812ca5fdc524faa', 'BA.Test', '¯«;%kîèÉ1t(¦-ˆú1—#³°á†U¹Êêõ*p'),
('LeninMary0', 'Mary Lenin', 1, '45d711f01f4ea0f64385eff73765894c1c5fc16b2b302625e146a83aa1df9aa0', 'BA.Test', '±9™#k˜®eØâŒ•¢úÌySÌ¬“ì£Ž« â\n6'),
('LeninMary173', 'Mary Lenin', 1, 'a684813c65ecc80fdceab56f20193196118f004441dabe67cb6f6f224638c2c5', 'BA.Test', '%,Ä.W‚8ÇàŸöÓ‡4mdðÎï\r’:Ø›—ET'),
('LeninMiranda88', 'Miranda Lenin', 1, 'ce9d12821f92795151412d5a62343004af083d930c3f270538d3dda221307d49', 'BA.Test', 'çÈbP‹ÛáÌš‹Ñ`d)ÑQýþÆ¤ðÀ¾>\Z†y¸‚ÚˆÆ'),
('LeninPaul113', 'Paul Lenin', 1, 'e6f00f9d154af2dc8a95eff5cf180104367d8ad712f717e6f008f79db980c619', 'BA.Test', 'Òót~4æ³ä!Já-Ô¼ï±wûtC\nJ§#î¥g'),
('LeninSam107', 'Sam Lenin', 1, 'f30d7f463e9ddce3931264b4cf9fbf8aecd34b9df324a7983d0e8651c15d1c0d', 'BA.Test', 'ÃÞáºø]rsg÷ÎŒSÏeF÷!åšdØCý=ÑÁ'),
('LeninTim147', 'Tim Lenin', 1, '51d8839f5d6122847b56214a27a671f4c7f04688de37b8838a1863b35f3fc19f', 'BA.Test', 'å7›ª4\ZŠšî ©ÙÉ$ê¢!“ŸnÅà0{l;ª•¦ë'),
('LeninTim19', 'Tim Lenin', 1, '8583e97df09e043d8805d5349a271e80cab0381d1f02ead24f7c279ec2df267c', 'BA.Test', 'Ú3vq¥{ºT§cÁÜ[#ÎÜ­.È/?ï•<Óëü-%Á'),
('LeninTim21', 'Tim Lenin', 1, 'c9b9d70480da09bae850ac627391c7f61d4640eb443bf0c3d245dfffe8f4da43', 'BA.Test', '_Üiä59''¯+\0²n‹¢®\0b²>Í²HYø7Û’I'),
('LeninTim76', 'Tim Lenin', 1, '514b289dd28c41d70a7f9051d793afd41bebcfffcdd9e9ec6df3749057fb1dbf', 'BA.Test', 'e,.z¶b—9nn²V&ŠÑ‡D‹÷ ›÷•C@Ü¶5s½'),
('LeninTom105', 'Tom Lenin', 1, 'e352153656fd8da87f7443cf974b589692b6341eaa4fe143ad202626a9b6d25d', 'BA.Test', 'c~õ©æ›üÔæj&P?èÊ¿$¡Ä¯½NZ¿'),
('LeninTom6', 'Tom Lenin', 1, 'edf73ce2e6a43dec6e1c2522876898d6fcad8dd4ab7b2202d2a249bc1be16ad5', 'BA.Test', '¬3Á3¡:xëºÛ¸\Z˜ÀÐæx/pK··&®’*'),
('MonkAmy28', 'Amy Monk', 1, '3a8178ebef26b306d474fc7175150eb3ef9c7311be8473bbb380748a707c173b', 'BA.Test', 'îj!\ZSÔW"ï±èE€è„$x3„ êÐ\\‡3ŠÇ?'),
('MonkCormac181', 'Cormac Monk', 1, '7a2325039e8e8f7643efbaeae8b7705d33fcb658efab29c7fb2f19f316d82214', 'BA.Test', 'F,‡Ü©-t‹õ4G<G`–Góï‘–»‘Z''·íµ'),
('MonkCormac20', 'Cormac Monk', 1, '953c743167596f9cb7d59fd9ddc0119851bcc6bc6fb89babb711fe2bc99963d4', 'BA.Test', 'cëž×Âálhirqòv¬à—¼YQ7ï5U©²'),
('MonkEmma191', 'Emma Monk', 1, 'c404544c4ecbf2173318be2f4032145619e4d2e415e8b438f82882bf1b65fcb5', 'BA.Test', '¨]â~Þq/ˆZB{‘—é4qÿFx¹mðS$.'),
('MonkFiona50', 'Fiona Monk', 1, '8eb99a36baab199846463f07bf61637fd6b3ccd5bd8f1ac2e4f488a4445c7ea1', 'BA.Test', '±,—\Z)\r^Å§’œE|òìP''©ô…´zRqÎàB{'),
('MonkFiona59', 'Fiona Monk', 1, 'f702441104c62f9b50bfb7fa5809c7f861d2ca6168a2c176b4ac14fb880c9048', 'BA.Test', '……eÊéˆÿïHÆÙ ÄÃcIQ×ìKÓµœÕ÷P„ÃÉ1'),
('MonkGlenn102', 'Glenn Monk', 1, '8df2017e97626de029026d89d0adb44f263d92197f31896f85bf5f46cf5b3967', 'BA.Test', '9{vÿöÑ¾ÓÜDÆ¶Ÿÿl´¼Ë‚S\rÅö±*Ã®þêS'),
('MonkKate69', 'Kate Monk', 1, 'e2286114f72a3233aebdef263acf81aa020a3f5b6c1c1ecd6ff4decbb3fb23d5', 'BA.Test', 'Âó~˜ë\n3é_þj1’\0Îu¶‡AŽ°õ›KC'),
('MonkMiranda188', 'Miranda Monk', 1, 'ff603198b934d138064a28548da39cb2f5a4057e66ebc9f932fda6a963a1b74e', 'BA.Test', '+ÐËOBâ¡AÆ€1],Îh­Œ©5| ŽÃóëÃ'),
('MonkPaul182', 'Paul Monk', 1, 'b159cf17228307b4b43db12ba444f9442bf308e03d97eae1484f1e1d788e7faa', 'BA.Test', '‹<%¬GoEêˆôÅ†…¡Ï“Ád©`¬Ö¯{  C–Ñ'' >i'),
('MonkSam78', 'Sam Monk', 1, 'ab4a1168cab8754641183a7d6259f37e3720db5dfb1e099b52c0ed7d698968b4', 'BA.Test', 'ÓæóE´rà¸h1ÿq•Tn§''èó¿ºzýÝÿ¹¤¯'),
('MonkTim98', 'Tim Monk', 1, 'c296ed6fa779cef237305004497ce0dc082e6242ca6c3f9566410af1cab89ef2', 'BA.Test', 'ÔZª7K‰ƒ‰ù>s(Éè\ZlNoðq>BR¸º.Wè'),
('MonkZeus184', 'Zeus Monk', 1, '919a36c8a255bf19179645ffc5631411eec0fc7a50eb59162f0c2f5d11b49dad', 'BA.Test', '¯ÌÔêDýÅú±rÎzw»Í·^X‡ýÅ7ò(ž'),
('MurphyAmy200', 'Amy Murphy', 1, '51a2374a9597f0391541a6dffb06e9d82d61dca338dea32b59ccfab691e7347e', 'BA.Test', '@ j1pqSádÊ_î sƒZþÕéÀÓÕ§€®õl¸´$*'),
('MurphyBob79', 'Bob Murphy', 1, '0532d9b4cd022de255a5ac0537c6d468079a722e164e2218d34f626f475cb80b', 'BA.Test', 'ÿEÊÔq{6ñèìÚËü“UÀyíÚ¶  )¥Ûk%-†'),
('MurphyEmma159', 'Emma Murphy', 1, 'ea6362d0cdeff95d38acbbf0f12e01f0dba9e0a64b529907af37e0fb455a9b85', 'BA.Test', 'Aý}—‚ ’ÐmÖQ)À’^­¶ü£}W¨ù<ŸÁ”¨0'),
('MurphyGlenn138', 'Glenn Murphy', 1, 'f1a3190d462fad67aec885486a1efe57dedf1b082f3edfdaa879ce34fd281e55', 'BA.Test', 'L5få\Z“öªVXÏt²yÓRË2Â ö¬o<1^Dí°'),
('MurphyGlenn38', 'Glenn Murphy', 1, 'dcc22acb3d109a6fddb97ce203ee664f293210015c14b279cda2d367dc274018', 'BA.Test', '\ZöKÄo3üƒ±="L\n¶â¡¸ëE0çþAä–{‹'),
('MurphyMiranda43', 'Miranda Murphy', 1, '0846b22cde77f03188e7d2978c8ebbb56df3fe07ecd6d520be0d7da0ab106523', 'BA.Test', 'a®Ç¨úé@ÜÎ¤*œ‘C‘h8E&3G×^c''z'),
('MurphyRichard4', 'Richard Murphy', 1, '9055f6dd4956291d86203aee6855c87a19674b07c4454effe0c63dd2254623ac', 'BA.Test', '¢ùø8nÝêfçµ$¥x8•\nÇønjt…tiRœ®{'),
('MurphySam183', 'Sam Murphy', 1, '53e5d1587014d09c6bbb0482b92c8243af9d71852b3a6e0c4477309fb9935113', 'BA.Test', '¢Öo»ÿŠ !¶N®Îi ÕüAI+»…ºjyþ°x'),
('MurphySam190', 'Sam Murphy', 1, '5149ffb49f102c2dd20ed0ba4a823b06efbaea70015a4bd9a64da93a1b084a39', 'BA.Test', '?Ã9Æ½ønûs–²ï÷‚cŽiOŒ&?ªà–‹š˜|v'),
('MurphySandra2', 'Sandra Murphy', 1, '45b58e0a0e82a46b186911eb2b035c866cdadc77825a95b08e45584570bc47c5', 'BA.Test', 'ñGœâ)Ô]ˆ‚ƒ±Üõ¯;ÈÔ3z)ÍÃ3—'),
('SharpAmy90', 'Amy Sharp', 1, '9b033a4bd1a12e0297bd47f79c76bf38afc5fa46dda177b5bfbf02935e4d0599', 'BA.Test', 'bAùäXÓ{öÜ¡†™·3éuVÁ6¾mo L(˜½Z©5'),
('SharpBob139', 'Bob Sharp', 1, 'b58da61905fcfbea550456f774a3dd0199c0c0ee65ba5e35173dd61bf36f1927', 'BA.Test', 'CúJ™ìpý\r’¹Ý\ZÈ†ìóÊ  u~ŒÙJ—%CQ—ýã'),
('SharpBob82', 'Bob Sharp', 1, 'e31ad9cdc1d2f2c52c2f7ccddf5a4dec8b9920050cacc555eb527956bd533a1f', 'BA.Test', 'l‘j¢ +sôÚ`)²XŒ9á¶ÁcŒèÏ¾ÁïÑ@u'),
('SharpCormac74', 'Cormac Sharp', 1, '7dac98511d5a133f283b09af624c4e20386c35e97c0e3982163de230d28736ee', 'BA.Test', ';zÚ`õµ¸Fü1¥ßjð=­U4s¬ìb*'),
('SharpCormac93', 'Cormac Sharp', 1, '8888356b2c08da7b94d241238c85b138ff779345f81e2c388140aa9ef74ba4c1', 'BA.Test', '''gïº©‘Ì·2ýAºw3\n24a,bxNò%(/—'),
('SharpFiona195', 'Fiona Sharp', 1, '0c884a34acbefd9b5ecdbce8e63040dee514139085dcbf31611522038addea3b', 'BA.Test', ' 5›?¾ióÄÍHÏq×Ñ–ÈV­ç‰tj­à³ÍZ†'),
('SharpFiona27', 'Fiona Sharp', 1, 'a829dde97a0bb8b3cbdb6fee20f2aac56b2447640c0c65cea1c5f45fb9135b86', 'BA.Test', 'L  X§\n—Ecßßrz\nC''''Åœðî5ñˆì¥s'),
('SharpJudas111', 'Judas Sharp', 1, 'b627611ad93e397c4d6868d936633d56155e18bfad170a40b3e105b129403113', 'BA.Test', '˜~J}*•÷ýíFgyôôBú¼±£s?w¨à§BÕJ'),
('SharpKate55', 'Kate Sharp', 1, '8d29b3829ab5dfd7ad8728fc9b2ebe542114f3771278ac946a6951de607af68d', 'BA.Test', 'Ø\0ë?Á''§Jd\ZÚ³øå‰ož*’ëàûDë‰ãÜ·ÑMÖ'),
('SharpMiranda157', 'Miranda Sharp', 1, '737c21b5133afa3b4d970251c2d31d93dd41e85aa3ff69027b4898d643aeda60', 'BA.Test', '«¢|pyÔÒU«±­w\rS^òë‰G£×¸\rÍÊ'),
('SharpSam1', 'Sam Sharp', 1, '89a6c09a687cd0ce309ef0093f1e00362e7fddc182e923958c8444d5dd97879a', 'BA.Test', 'q4¸h5\0ýk%—7ºõ¡(’÷¤<ö°¬³­ÎQ2•â'),
('SharpTim14', 'Tim Sharp', 1, '3d7d7921e5188bd85423ea34d5566282270631eb6cc0053aff8d2f9c40a9edde', 'BA.Test', '´¡‘Õ¼$ˆÌ†,!‹p›¶þÑî³-öÜÐëž"›†L'),
('SharpZeus60', 'Zeus Sharp', 1, '5ddbd11910ab9f53936ff9ea9331a765909e65c2afd9059df58f4f258da72d83', 'BA.Test', '{Ì)Mó*es²9Ýƒ\0GRºLw¤·­µb-$qì!7$'),
('StalinFiona10', 'Fiona Stalin', 1, '68f406d5ce8d6e524343ceb9c744a452c3aeafd9d9b6eeadc5a8d0d6b2f295c2', 'BA.Test', 'HFÅÉ~_•pd“±{-^_HÈñz:%rddÁ£nWÄ'),
('StalinJack99', 'Jack Stalin', 1, 'dd3f27444ca90ddaaec3c7823e59f9b26345f6714e8dabd2fb16f36de498bff1', 'BA.Test', 'L\Zê’¬ž;¢‘’?›ÊA»¢½ë3•!eõ“ ”E'),
('StalinMary66', 'Mary Stalin', 1, '6470c6521108193144309ec9c63e72c92b7522c4c63976fd1f966bce8f54cddb', 'BA.Test', 'F•ÊvžÐŸ  ªuUpÀ"_l-“2â°''y„fúŽK…Š4'),
('StalinMiranda106', 'Miranda Stalin', 1, 'a3c8a3a40057f32dfed9622a12a39e6003becf82802af9f6b13988b6bc137edd', 'BA.Test', 'µÄè®fÿ, IˆAÐßšüm>Ú÷T-ÕH(6à'),
('StalinMiranda143', 'Miranda Stalin', 1, '2faa7ebacd20b61903e2ab6ed6275f71be48e07c294fc9e44c92337f63631298', 'BA.Test', 'ºö''*i“3Œ0mŠÁ%yÈ©×{û  üe*lJ?4Wþ'),
('StalinPaul83', 'Paul Stalin', 1, '92f9f81299dabb97b2fda5441fb1b383920ea91b378d12bd6bca8882b98b3cc5', 'BA.Test', 'Ãä§¿l°ÅrÄê±2®\ZGG[ÐMåä‡šó÷RÝÕ'),
('StalinSam18', 'Sam Stalin', 1, '695526f90b9760d1df448b237086847e9234e65d68550b004740b512cb24798e', 'BA.Test', ':\rµæEgfn¨Øj’Gç=h •9 |InRNTÌ'),
('StalinSam85', 'Sam Stalin', 1, 'c71e080208d5651bc55bd50685c4a10325471577520fc2aa5eb8cb0079ce9390', 'BA.Test', '49§_z0˜§{P=ÞØÛk“lös''½(¼ûK2·ä'),
('StallmanCormac133', 'Cormac Stallman', 1, 'e24802b2cc7f93421a3681e061b9d3b33ee4b0f3a5f6ceeb6221e7e78a6b5f1a', 'BA.Test', 'sBFGÓ’(Üõdc$ÌC_¹Ö{¡š…|a^p'),
('StallmanKate145', 'Kate Stallman', 1, 'ec98ebb8f523ffc242d7cf2051f7ded77b46c0f6f8ae1413075a224cf5bc508b', 'BA.Test', 'ÿ‹%èOxz¼2u/™Ò ”ÒL!ïH ‡Rë''"f1¦×'),
('StallmanKate47', 'Kate Stallman', 1, '12deb7023ee9a574d4a874e932acf06fdfd3dc10f02aa210d7e450ac88ad27bc', 'BA.Test', 'Ý#™(%ÇrÀh—‘‰ßçë¢qj*vÕ­’¶Y'),
('StallmanMary65', 'Mary Stallman', 1, '92649cbf4399e89d1456e8b4a87b6d92ff643318d96a9246240ee030ca43512a', 'BA.Test', '•G¯tÙzÚÞO‰£t«[*šÄ®É²å¦Ô*Mú\0>'),
('StallmanMiranda119', 'Miranda Stallman', 1, '1040898ad5d9b2e5ec20b3803844017713867c68254c39ac2068f9599b80d276', 'BA.Test', 'Zj~M5Z§iW0³pfñù5…‘š$K‰“)&»•¶'),
('StallmanMiranda44', 'Miranda Stallman', 1, '14a6658d5fcdeffefde41f0693245716e89eaf9c319944f7d61817da86db8d41', 'BA.Test', 'Ë¡4Ã&èÞ+¡€ÅÍA&®Yh}ÆÍõ£Z"k¸úú'),
('StallmanPaul22', 'Paul Stallman', 1, '4248a682edfbfff90e851cbef548de04bf7345c09ed73c447e36722046e84623', 'BA.Test', '.}AHúÓÁ„:\0×ýWz„ééçnD¶PO}„Z%'),
('StallmanRichard49', 'Richard Stallman', 1, 'de3c3dad914db5da9c9b57bf0f6eaaf32395ff323079392b7b814bd2c7599765', 'BA.Test', 'wØµ¸ª“eeo¥)ç¾ih×°ÃæùçísÍî{è'),
('StallmanSam180', 'Sam Stallman', 1, '6e5f2a21dea9f1cc6477ad3895c3f1d1c949338ad5482ecde33f189d35e45998', 'BA.Test', '…Á0¬†JÎack4‡Î“4Ûó~ @åSï'),
('StallmanTim17', 'Tim Stallman', 1, 'acbedffd397f95d710cca8eb2db213fa5e4f2733c837828a7d12d386349d5623', 'BA.Test', 'sîÎ&óÌ3z³;2= /6’G3›g<ä!Ñ ÞY§¸'),
('StallmanTim35', 'Tim Stallman', 1, 'de9897fdf84b19c11c0463e7dde8f5e04d41182e5907ab16c729e89efa546865', 'BA.Test', 'âË“ŸV5PŠ‡˜áÇx¤h ×£"sõcÃwpÃî'),
('StallmanTim86', 'Tim Stallman', 1, 'ee70d8d5ef258b39612f4dbc453f0e07afc77f38461925e38aa50ea006e0d4c8', 'BA.Test', '×Íå¯‹¥¾£›ÊØ:åRVfynÙ!–xÛur%>n2'),
('TrotsyAmy187', 'Amy Trotsy', 1, 'bd466c279737033b51bbfe6e4986c46c69eebb6b1b0419da7bcc2b007446b6bf', 'BA.Test', 'ÔiÐeÍ1Ú‡X¦‚E=Û#ÒíöKT‰''J»…0ü'),
('TrotsyEmma103', 'Emma Trotsy', 1, '71683efd94bf8d50491bab17da8e92704e8d5b539033c2e8bf91f42fd4faf0de', 'BA.Test', 'L|§[ªYÐFÐ´Ì„ÝÁ^|þ‹6®”ÜPü†á'),
('TrotsyEmma144', 'Emma Trotsy', 1, '7cb11808130fd806397ecd217493c11306047d290a879bdcdba0a8b8c70bccc0', 'BA.Test', '\0•æï±À\\ˆ8<ür?y`ºD’ø’·Á;¶Ó‹ºÃ'),
('TrotsyFiona135', 'Fiona Trotsy', 1, 'dbe4ad44de86aaea8f97d67317a114beb7194b5f5bff7bb4719f26adce4e6dae', 'BA.Test', 'üu!¨Aê^³*×9­ËŸ‹7<ý¦‰m¹—x'),
('TrotsyFiona24', 'Fiona Trotsy', 1, '477dbbd7fbf6e11eaf7cd662df676d8d7c3a5042d5c126fbe24c83c6066fc300', 'BA.Test', '÷™”Ø÷^X`tjP2ƒEÜì­Âû¦5„µrD!½2©'),
('TrotsyGlenn132', 'Glenn Trotsy', 1, 'b5f0dc710ede70de1a3e6e6953e0b7b09fc807ccf63b47d1ff01c885f14b7ec1', 'BA.Test', 'z”ñÞçÑ…¥MÈë¾vÌ´gÈ¿‘0å·6Ãnzl­Š'),
('TrotsyMary58', 'Mary Trotsy', 1, '63f4e7959f317f8c6d1a11c07e7ca5dbc7bcf0efe3f0ef324aee8d48fa620d53', 'BA.Test', '^S\nId\nÜ˜vigPè—r™AÍåêþÜ×—´îØ'),
('TrotsyMiranda62', 'Miranda Trotsy', 1, '53762f2c1eadf45da6b780e23e36db5c30593921a537c6fc9104119a566cc4bb', 'BA.Test', 'Øèü ¦4eNÃ\råqÓ:¼¯ÏÕ«”&EAJí¿N5s\r³'),
('TrotsySam155', 'Sam Trotsy', 1, 'a51c42c52cfd3cc2440181fc15ab9f14b391512d7e0ef0d31bf91b20c823f8de', 'BA.Test', 'n1}fU@ó%n$!­ž,³ÄAÿ\ZÝ{½2(?~V7'),
('TrotsyZeus134', 'Zeus Trotsy', 1, 'dd83391df3fcb1c6e8af7881afdbff32445cdee6fc731acf492fb0643a081da8', 'BA.Test', '´î}†øî¹¿šø»…µw·Æ±Š7Ë$,+t8U'),
('TrotsyZeus137', 'Zeus Trotsy', 1, '45bb89a2b6d4182e3366fee810e627632d76813d969649875a2240d3d1e9afc4', 'BA.Test', '¢¥Ú•h±.}Óïsèü®%Iº©»~ac½s½_');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `user_id` varchar(64) NOT NULL,
  `group_id` int(11) NOT NULL,
  `owner` tinyint(1) NOT NULL COMMENT 'The person who created the group',
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`user_id`, `group_id`, `owner`) VALUES
('BannerKate131', 3, 1),
('BannerKate131', 4, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_ibfk_10` FOREIGN KEY (`user`) REFERENCES `users` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `bookings_ibfk_8` FOREIGN KEY (`area`) REFERENCES `areas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `bookings_ibfk_9` FOREIGN KEY (`room`) REFERENCES `rooms` (`number`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`area_number`) REFERENCES `areas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD CONSTRAINT `user_groups_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_groups_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 05, 2013 at 09:48 AM
-- Server version: 5.5.25
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `bookingdata`
--

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE `areas` (
  `ID` int(11) NOT NULL,
  `no_rooms` int(11) NOT NULL,
  `details` varchar(128) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `blacklist`
--

CREATE TABLE `blacklist` (
  `id` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL COMMENT 'The name of the blacklisted user',
  `start_time` datetime NOT NULL COMMENT 'Start time of the ban',
  `end_time` datetime NOT NULL COMMENT 'End time of the ban',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `time` datetime NOT NULL,
  `user` varchar(64) NOT NULL DEFAULT '',
  `area` int(11) NOT NULL,
  `room` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `purpose` varchar(130) NOT NULL,
  `confirmed` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'confirmed boolean',
  `group_booking` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Is it a group booking?',
  PRIMARY KEY (`user`,`time`),
  UNIQUE KEY `time` (`time`,`room`),
  KEY `area` (`area`),
  KEY `room` (`room`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `id_2` (`id`),
  UNIQUE KEY `id_3` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_profile` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Decides whether the current profile has admin privileges',
  `Name` varchar(64) NOT NULL,
  `Description` varchar(140) NOT NULL COMMENT 'Brief description of profile',
  `Booking_limit` int(10) unsigned NOT NULL COMMENT 'Max number of days in advance that a booking can be made',
  `Max_number_bookings` int(10) unsigned NOT NULL COMMENT 'Max number of bookings that this user can make. Infinite if null',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Name` (`Name`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `number` int(11) NOT NULL,
  `area_number` int(11) NOT NULL,
  `capacity` int(11) NOT NULL,
  `monitor` tinyint(1) NOT NULL,
  `other` varchar(256) NOT NULL,
  PRIMARY KEY (`number`,`area_number`),
  KEY `area_number` (`area_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(64) NOT NULL,
  `name` varchar(70) NOT NULL,
  `profile` int(11) NOT NULL,
  `password` char(64) NOT NULL COMMENT 'SHA256 password',
  `courseCode` varchar(10) NOT NULL,
  `salt` char(32) NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `username` (`username`),
  KEY `username_2` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `user_id` varchar(64) NOT NULL,
  `group_id` int(11) NOT NULL,
  `owner` tinyint(1) NOT NULL COMMENT 'The person who created the group',
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_ibfk_10` FOREIGN KEY (`user`) REFERENCES `users` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `bookings_ibfk_8` FOREIGN KEY (`area`) REFERENCES `areas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `bookings_ibfk_9` FOREIGN KEY (`room`) REFERENCES `rooms` (`number`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`area_number`) REFERENCES `areas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD CONSTRAINT `user_groups_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_groups_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION;

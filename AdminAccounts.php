<?php
/*
*     Page that allows the admin to view all users and it gives them the option to remove a blacklisted user
*/
require_once( "common.inc.php" );
checkedLoggedIn();
checkAdminAccount();
insertStandardHTML( "Admin Account" );
$pagenumber = 0;

if(isset($_POST["Next"]) ||  isset($_POST["Previous"])) //Change page
{
  $pagenumber = $_POST["PageNumber"];
  $pagenumber = nextPageNumber($pagenumber);
}
else if(isset($_POST["Remove_Blacklist"]) && isset($_POST["Student_UserName"])) //If the user chooses to Blaclist a user.
{
  RemoveFromBlacklist( $_POST["Student_UserName"] );
}
displayPage($pagenumber);

function displayPage($pagenumber)
{
?>
  <form action="AdminAccounts.php" method="post">
      <?php
      SearchAccounts($pagenumber);
      ?>
      <input type="submit" name="Previous" value="Previous" id="Previous"></input>
      <input type="submit" name="Next" value="Next" id="Next"></input>
      <input name="PageNumber" type="hidden" value=<?php echo $pagenumber ?> id="PageNumber"> </input>
   </form>
  <h1> <?php echo ($pagenumber+1) ?> </h1>

<?php
}


//Set the time of the blacklisted person to the current time to remove them from the list.
//This way, it is recorded that the person was at one stage blacklisted.
function RemoveFromBlacklist($username){ 
  $databaseConnection = getDatabaseConnection();
  try
  {
    //Generate current time.
    $time = time() - (time() % 3600);         //Rounds down to the start of the hour
    $time = date("Y-m-d H:i:s", $time);
    $query = "UPDATE `blacklist` SET `end_time` = :time WHERE `username` = :username AND `end_time` > :time";
    $connection = $databaseConnection -> prepare($query);
    $connection -> bindValue(':time', $time, PDO::PARAM_STR );
    $connection -> bindValue(':username', $username, PDO::PARAM_STR );
    $connection -> execute();
    $databaseConnection = "";
  }
  catch ( PDOException $e )
  {
    $databaseConnection = "";
    echo "Error Occured: " . $e->getMessage();
    die();//Close the connection.
  }
}

function BlacklistForm($Student_UserName){

  if(isUserBlacklisted($Student_UserName))      //If they are on the Blacklist, and you want to remove them
  {
    ?>
    <form action="AdminAccounts.php" method="post">
      <input name="Remove_Blacklist" type="submit" id="Remove_Blacklist" value="Remove from blacklist"></input>
      <input name="Student_UserName" type="hidden" id="Student_UserName" value=<?php echo $Student_UserName; ?>></input>
    </form>
  <?php
  }

}

function SearchAccounts($pagenum){//Print out the table that displays all users, and current booings.

	try
  	{
    	$databaseConnection = getDatabaseConnection();
  	}
    catch ( PDOException $e ) 
  	{
  	  echo "Connection failed: " . $e->getMessage();
  	}
  	try //Print out the information.
  	{
      //Generate time
      $time = time() - (time() % 3600);         //Rounds down to the start of the hour
      $time = date("Y-m-d H:i:s", $time);
      $lastname = "";
      $total = 100;//Display 100 results per page.
      $start = ($total * $pagenum);

      //Get the username and name from the database.
  		$query = "SELECT * FROM `users`
                LEFT JOIN `bookings`
                ON bookings.user = users.username limit $start, $total"; 
      
      $connection = $databaseConnection -> prepare( $query );
      $connection -> execute();
      $result = $connection -> fetchall();

  		echo "<table>";
      echo "<tr><td> User </td><td> Status </td><td> Upcoming Bookings for User";
      //Prints out each user, and if they have upcoming bookigs.
      //The if statements are there because of the way the table is read in.
      //They stop usernames being printed twice, and times are all displayed in one box.
  		foreach($result as $row)
  		{
        if($row['username'] != $lastname)
        {
			    echo " </td></tr><tr><td>" . $row['username'] . "</br>" . $row['name'] . "</td> <td> Status: ";
          if($row['profile'] == 1)
          {
            if(isUserBlacklisted($row['username']))//Check if the Profile is Blacklisted
            {
              echo "Blacklisted";
            }
            else
            {
              echo "Student";
            }
            //Show form to Blacklist people.
            BlacklistForm($row['username']);
          }
          else if($row['profile'] == 2)
          {
            echo "Admin";
          }
          else
          {
            echo "Unknown";
          }
          echo "</td> <td>";
        }
        if($row['time'] >= $time)
        {
          echo $row['time'] . " in room: " . $row['room'] . "  for purpose: " . $row['purpose'] . "</br>";
        }
        $lastname = $row['username'];
  		}
  		echo " </td></tr></table>";
  		$databaseConnection = "";//Close connection.
  	}
    catch ( PDOException $e )
  	{
  		$databaseConnection = "";
  		echo "Error Occured: " . $e->getMessage();
  		die();//Close the connection.
  	}
}

displayFooter();
?>
<?php
/* 
*   Page that is used to blacklist a user
*/ 
require_once( "common.inc.php" );
checkedLoggedIn();
checkAdminAccount();
insertCalendarHTML( "Add user to Blacklist" );

/*
*   Checks to see if the submit button has been pressed and a day, month, year, hour and username has been passed back by the form
*   Process the data if it was sent, otherwise it displays an error message and redisplays the date select page.
*/
if ( isset( $_POST["submitButton"] ) ) {
  if(isset($_POST["year"]) && isset($_POST["month"]) && isset($_POST["day"]) && isset($_POST["hour"]) && isset($_POST["username"]))
  {
    processBlacklist();
  }
  else
  {
    echo "Username or date invalid";
    displayForm();
  }
} 
else {
  displayForm();
}


/*
*   Process the date returned using POST.
*   Coverts the date into a timestamp that the MySQL server will accept and then blacklists the user until the timestamp time is reached
*/
function processBlacklist() 
{
    $date = $_POST["year"] . "-" . $_POST["month"] . "-" . $_POST["day"] . " " . $_POST["hour"] . ":0:0";
    $date = strtotime($date);  
    $date = date("Y-m-d H:i:s", $date);
    $username = $_POST["username"];
    blacklistUser($date, $username);
}


/*
*   Diplays the form that allows the admin to select a date and choose a username.
*/
function displayForm() 
{
  ?>
    <h1>Blacklist a user:</h1>
        <?php displayDateOptions("blacklist_user.php");
        displayHours(); ?>
        <p>
          <label for="username">Username</label>
          <input type="text" name="username" id="username" maxlength="129"/></td>         
        </p>
        <div style="clear: both;">
          <input type="submit" name="submitButton" id="submitButton" value="Send Details" />
          <input type="reset" name="resetButton" id="resetButton" value="Reset Form" style="margin-right: 20px;" />
        </div>
      </div>
    </form>
<?php
}


/*
*     Function that receives a date and a username and blacklists that user until the specified date.
*/
function blacklistUser($date, $username)
{
  $start_time = time();
  $start_time = date("Y-m-d H:i:s", $start_time);                                                             // Converts the current time(start time) into a timestamp
  $sql = "INSERT INTO blacklist (`username`, `start_time`, `end_time`) VALUES (:username, :start, :end)" ;    // Clears table
  $databaseConnection = getDatabaseConnection();
  try 
  {
    $connection = $databaseConnection->prepare( $sql );
    $connection-> bindValue( ":username", $username, PDO::PARAM_STR );
    $connection-> bindValue( ":start", $start_time, PDO::PARAM_STR );
    $connection-> bindValue( ":end", $date, PDO::PARAM_STR );
    $connection-> execute();
    echo $username . " has been blacklisted until " . $date;
  }
  catch (PDOException $e) 
  {
    $databaseConnection = "";            //closes connection  
    echo "Error: " . $e->getMessage();                 
    die ();
  } 

}

displayFooter();
?>
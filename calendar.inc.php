<?php
	require_once( "common.inc.php" );
	error_reporting(E_WARNING);
	$cal_ID = 0;  // the current calendar

	class CALENDAR {

		var $borderColor = '#304B90';			// border color
		var $offset = 2;						// week start: 0 Monday
		var $weekNumbers = true;				// view week numbers

		//days
		var $weekdays = array("Sat", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri");

		// months
		var $months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

		var $year, $month, $size;
		var $mDays = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31); // days in the months
		var $specDays = array();
		var $specDays2 = array();


		function CALENDAR($year = '', $month = '', $week = '') {
			if($year == '' && $month == '') {
				$year = date('Y');
				$month = date('n');
			}
			else if($year != '' && $month == '') $month = 1;
			$this->year = (int) $year;
			$this->month = (int) $month;
			$this->week = (int) $week;
			if($this->linkTarget == '') $this->linkTarget = 'document';
		}

		function set_styles() {
			global $cal_ID;

			$cal_ID++;
			// title
			$html = '<style> .cssTitle' . $cal_ID . ' { ';
			$html .= 'font-family: Arial, Helvetica; ';
			$html .= 'font-size: ' . 14 . 'px; ';
			$html .= 'color: #000000; ';
			$html .= 'background-color: #FFFFFF; ';

			// headings
			$html .= '} .cssHeading' . $cal_ID . ' { ';
			$html .= 'font-family: Arial, Helvetica; ';
			$html .= 'font-size: ' . 10 . 'px; ';
			$html .= 'color: #000000; ';
			$html .= 'background-color: #FFFFFF; ';

			// days set to white by default
			$html .= '} .cssDays' . $cal_ID . ' { ';
			$html .= 'font-family: Arial, Helvetica; ';
			$html .= 'font-size: ' . 10 . 'px; ';
			$html .= 'color: #000000; ';
			$html .= 'background-color: #FFFFFF; ';

			$html .= '} </style>';

			return $html;
		}

		function is_leap_year($year) {
			if (!($year % 4) && ($year < 1582 || $year % 100 || !($year % 400))){
				return true;
			}else{
				return false;
			}
			
		}

		function get_weekday($year, $days) {
			$a = $days;
			if($year){
				$a = $a +  ($year - 1) * 365;
			}
			for($i = 1; $i < $year; $i++){
				if($this->is_leap_year($i)){
					 $a++;
				}
			} 
			if($year > 1582 || ($year == 1582 && $days > 277)){
			 	$a = $a - 10;
			}
			if($a){
				$a = ($a - $this->offset) % 7;
			}
			else if($this->offset) {
				$a += 7 - $this->offset;
			}

			return $a;
		}

		function get_week($year, $days) {
			$firstWDay = $this->get_weekday($year, 0);
			if($year == 1582 && $days > 277) {
				$days = $days - 10;
			}
			$theweek = floor(($days + $firstWDay) / 7) + ($firstWDay <= 3);
			return $theweek;
		}

		function table_cell($content, $class, $date = '', $style = '') {
			global $cal_ID;

			$size = round($this->size * 1.5);
			$html = '<td align=center width=' . $size . ' class="' . $class . '"';

			// if not a blank cell 
			if($content != '&nbsp;' && stristr($class, 'day')) {	
				$countbookings = getDayBookings($date); // get the amount of bookings for that date

				$html .= ' title=""';
				$style .= 'background-color:' . chooseColour($countbookings) . ';';  // set the colour for the number of bookings
				
			}
			if($style) $html .= ' style="' . $style . '"';
			$html .= '>' . $content . '</td>'; // put date in the cell

			return $html;
		}

		function table_head($content) {
			global $cal_ID;

			if($this->weekNumbers){
				$cols = 8;
			}else{
				$cols = 7;
			}
			$html = '<tr><td colspan=' . $cols . ' class="cssTitle' . $cal_ID . '" align=center><b>' .
					$content . '</b></td></tr><tr>';
			for($i = 0; $i < count($this->weekdays); $i++) {
				$ind = ($i + $this->offset) % 7;
				$wDay = $this->weekdays[$ind];
				$html .= $this->table_cell($wDay, 'cssHeading' . $cal_ID);
			}
			if($this->weekNumbers) $html .= $this->table_cell('&nbsp;', 'cssHeading' . $cal_ID);
			$html .= '</tr>';

			return $html;
		}


		function create() {
			global $cal_ID;
			// set up current month
			$this->size = ($this->hFontSize > $this->dFontSize) ? $this->hFontSize : $this->dFontSize;
			if($this->wFontSize > $this->size) $this->size = $this->wFontSize;

			list($curYear, $curMonth, $curDay) = explode('-', date('Y-m-d'));

			// error checking
			if($this->year < 1 || $this->year > 3999){
				 $html = '<b> Year must be 1 - 3999! </b>';
			}
			else if($this->month < 1 || $this->month > 12){
				 $html = '<b> Month must be 1 - 12!</b>';
			}
			else {
				$this->mDays[1] = $this->is_leap_year($this->year) ? 29 : 28; // set feb to leap year or not
				for($i = $days = 0; $i < $this->month - 1; $i++) {
					$days += $this->mDays[$i]; // count all the days
				}
				
				$start = $this->get_weekday($this->year, $days);
				$stop = $this->mDays[$this->month-1]; // find how many days in this month

				$html = $this->set_styles(); // call styling from above
				$html .= '<table border=0 cellspacing=0 cellpadding=0><tr>';
				$html .= '<td' . ($this->borderColor ? ' bgcolor=' . $this->borderColor	: '') . '>';
				$html .= '<table border=0 cellspacing=1 cellpadding=0>';
				$title = htmlentities($this->months[$this->month-1]) . ' ' . $this->year; // title of calendar
				$html .= $this->table_head($title);
				$daycount = 1;

				if(($this->year == $curYear) && ($this->month == $curMonth)) {
					$inThisMonth = true;
				}else {
					$inThisMonth = false;
				}

				if($this->weekNumbers || $this->week){
					 $weekNr = $this->get_week($this->year, $days);
				}
				

				while($daycount <= $stop) {
					if($this->week && $this->week != $weekNr) {
						$daycount += 7 - ($daycount == 1 ? $start : 0);
						$weekNr++;
						continue;
					}
					$html .= '<tr>';

					for($i = $wdays = 0; $i <= 6; $i++) {
						$ind = ($i + $this->offset) % 7;
						$class = 'cssDays';

						$style = '';
						$date = sprintf('%4d-%02d-%02d', $this->year, $this->month, $daycount);

						if(($daycount == 1 && $i < $start) || $daycount > $stop) $content = '&nbsp;';
						else {
							$content = $daycount;
							if($inThisMonth && $daycount == $curDay) {
								$style = 'padding:0px;border:3px solid ' . $this->tdBorderColor . ';';
							}
							else if($this->year == 1582 && $this->month == 10 && $daycount == 4) $daycount = 14;
							$daycount++;
							$wdays++;
						}
						$html .= $this->table_cell($content, $class . $cal_ID, $date, $style);
					}

	
					$html .= '</tr>';
				}
				$html .= '</table></td></tr></table>';
			}
			return $html;
		}
	}
?>

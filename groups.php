<?php
/*
*     Page that allows a user to edit their groups
*/
require_once( "common.inc.php" );
checkedLoggedIn();
insertStandardHTML( "Group Managment" );

$username = $_SESSION["user"]->getValue("username");
if (isset( $_POST["groupName"] ) ) {
	$groupNumber = $_POST["groupName"];
	if(isset( $_POST["addUser"] ))
	{
		// check user exists
		// check not already in the group
		$user = $_POST["username"];
		processAddUser($user, $groupNumber);
	}
	else if(isset( $_POST["deleteUser"] ))
	{
		// check user exists
		// check not already in the group
		$user = $_POST["deletedUser"];
		removeUserFromGroup($user, $groupNumber);
	} 
	displayGroupOptions($groupNumber);
  // If submitted process form.
} 
else
{
	displaySelectForm($username);
}

/*
*     Displays the form that displays the groups that the current user is the owner of
*/
function displaySelectForm($username) {
	$groups = getUsersGroups($username);
  	// check they are in charge of a group
  	if(count($groups) > 0)
  	{	
    ?>
    	<h2>Select the group you wish to edit</h2>
        <form action="groups.php" method="post">
        	<?php
        	// loop through groups
        	?>
        	<table>
        		<tr><td>Group Name</td><td></td></tr>
        		<?php
	        	foreach($groups as $group)
	        	{
		        	?>
		           	<tr>
                  <td><?php echo $group["name"] ?></td><td><input type="radio" name="groupName" value= <?php echo $group["group_id"] ?> ></td>
                </tr>
		           	<?php
	           	}
	           	?>
	        </table>
         	<div style="clear: both;">
           		<input type="submit" name="groupSelect" id="groupSelect" value="Send Details" />
        	</div>
        </form>
     <?php
 	}
 	else
 	{
 		echo "<h1>You are not the owner of any groups!<h1>";
 	}
}

/*
*     Displays group specific options
*/
function displayGroupOptions($groupNumber)  {
	?>	
	<form action="groups.php" method="post">
		<?php displayGroupDetails($groupNumber); ?>
		<input type="hidden" name="groupName" id="groupName" value="<?php echo $_POST["groupName"]?>"/>
    	<div style="width: 30em; padding-left: 10px;">
			<h2>Add a user:</h2>
			<p>Username<input type="text" name="username"></p>
			<div style="clear: both;">
            	<input type="submit" name="addUser" id="addUser" value="Add User" />
        	</div>
		</div>
	</form>
	<?php
}

/*
*     Function that display
*/
function displayGroupDetails($groupNumber)  
{
	$sql = "SELECT * FROM `user_groups` WHERE `group_id` = :groupNumber";
	$databaseConnection = getDatabaseConnection();
	try {
      $connection = $databaseConnection->prepare( $sql );
      $connection-> bindValue( ":groupNumber", $groupNumber, PDO::PARAM_INT );
      $connection-> execute();
      $Queryresult = $connection->fetchAll();
    }
    catch (PDOException $e) {
      $databaseConnection = "";            //closes connection
      echo $e->getMessage();                 
    }
    ?>
    <h2>Group Members</h2>
    <table>
    	<tr><td>Name</td><td>Group Owner</td><td></td></tr>
    <?php
    foreach($Queryresult as $user)
    {
    	?>
    	<tr>
    	<td><?php echo $user["user_id"]?></td><td><?php echo ($user["owner"] == 1 ? "Yes" : "No")?></td><td><input type="radio" name="deletedUser" value= <?php echo $user["user_id"]; ?>></td>
    	<tr>
    	<?php
    }
    ?>
    </table>
    <input type="submit" name="deleteUser" id="deleteUser" value="Delete User">
    <p></p>
    <?php
}

function processAddUser($user, $groupNumber)
{
	if(User::doesUserExist($user))
		{
			if(UserAlreadyInGroup($user, $groupNumber))
			{
				echo $user . " is already in the group ";
			}
			else
			{
				addUser($user, $groupNumber);
			}
		}
	else
	{
		echo $user . " does not exist";
	}
}

function addUser($user, $groupNumber)
{	
	addUserToGroup($groupNumber, $user, False);
}

function UserAlreadyInGroup($username, $groupNumber)
{
	$sql = "SELECT * FROM `user_groups` WHERE `user_id` = :username AND `group_id` = :groupNumber";
	$databaseConnection = getDatabaseConnection();
	try {
      $connection = $databaseConnection->prepare( $sql );
      $connection-> bindValue( ":username", $username, PDO::PARAM_STR );
      $connection-> bindValue( ":groupNumber", $groupNumber, PDO::PARAM_INT );
      $connection-> execute();
      $Queryresult = $connection->fetchAll();
    }
    catch (PDOException $e) {
      $databaseConnection = "";            //closes connection
      echo $e->getMessage();                 
    }

    return count($Queryresult) > 0;
}

function removeUserFromGroup($user, $groupNumber)
{
	$sql = "DELETE FROM `user_groups` WHERE `user_id` = :username AND `group_id` = :groupNumber AND `owner` != 1";
	$databaseConnection = getDatabaseConnection();
	try {
      $connection = $databaseConnection->prepare( $sql );
      $connection-> bindValue( ":username", $user, PDO::PARAM_STR );
      $connection-> bindValue( ":groupNumber", $groupNumber, PDO::PARAM_INT );
      $connection-> execute();
    }
    catch (PDOException $e) {
      $databaseConnection = "";            //closes connection
      echo $e->getMessage();                 
    }
}

displayFooter();
?>
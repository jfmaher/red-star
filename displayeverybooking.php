<?php
/*
*   Function that displays everybooking and allows the user to refine their search. Admin can also delete bookings
*/
require_once( "common.inc.php" );
checkedLoggedIn();
checkAdminAccount();
insertStandardHTML( "View Bookings" );
$pagenumber = 0;
$query = "";
$name = "";
$room = "";
$day = "";
$month = "";
$year = "";


if(isset($_POST["Next"]) ||  isset($_POST["Previous"]))//Change page
{
  $pagenumber = $_POST["PageNumber"];
  $pagenumber = nextPageNumber($pagenumber);
}
else if( isset( $_POST["submit"] ) )//If wanted to view only certain results.
{
  $name = $_POST["User"];
  $room = $_POST["Room"];
  $day = (int)$_POST["Day"];
  $month = (int)$_POST["Month"];
  $year = (int)$_POST["Year"];
  $query = searchQuery($name, $room, $day, $month, $year);
  $pagenumber = 0;
}
else if(isset($_POST["deleteSubmit"]) && isset($_POST["delete"]))//If the Admin wants to delete a users booking
{
  deleteBookings($_POST["delete"]);
}
else
{
  $pagenumber = 0;
}
displayPage($pagenumber, $query, $name, $room, $day, $month, $year);

function displayPage($pagenumber, $query, $name, $room, $day, $month, $year)
{
?>
  <!-- Display only wanted bookings-->
  <form action="displayeverybooking.php" method="post">
    Day: <select name="Day" id="Day"> <?php SelectDay();?></select> <!-- Day -->
    Month: <select name="Month" id="Month"> <?php SelectMonth(); ?> </select> <!-- Month -->
    Year: <select name="Year" id="Year"> <?php SelectYear(); ?> </select> <!-- Year -->
    </br>
    Room: <select name="Room" id="Room"> <?php SelectRoom(); ?> </select> <!-- Room -->
    Username: <input name="User" type="text" value="" id="User"></input> <!-- User -->
    </br>
    <input name="submit" type="submit" id="submit"></input>
  </form>

  <form action="displayeverybooking.php" method="post">
    <?php 
    if($query)
    {
      displaySearchedBookings($query, $name, $room, $day, $month, $year);
    }
    else
    {
      displayEveryBooking($pagenumber);
    }
    ?>
  </form>

  <!-- Choose page -->
  <form action="displayeverybooking.php" method="post">
    <input type="submit" name="Previous" value="Previous" id="Previous"></input>
    <input type="submit" name="Next" value="Next" id="Next"></input>
    <input name="PageNumber" type="hidden" value=<?php echo $pagenumber ?> id="PageNumber"> </input>
 </form>
  <h1> <?php echo ($pagenumber+1) ?> </h1>
<?php
}

function SelectDay(){ //Options to select day for form
  ?>
  <option value=""></option>
  <?php
  for($i = 1; $i <= 31; $i++)
  { ?>
    <option value=<?php echo $i; ?> > <?php echo $i; ?> </option>
    <?php
  }
}

function SelectMonth(){ //Options to select Month for above form
  $month = array("January","February","March","April","May","June","July","August","September","October","November","December");
  ?>
  <option value=""></option>
  <?php
  for($i = 0; $i < 12; $i++)
  { 
    ?>
    <option value=<?php echo $i +1; ?> > <?php echo $month[$i]; ?> </option>
    <?php
  }
}

function SelectYear(){ //Options to select year for above form
  ?>
  <option value=""></option>
  <?php
  for($i = 1990; $i <= 2020; $i++)
  { 
    ?>
    <option value=<?php echo $i; ?> > <?php echo $i; ?> </option>
    <?php
  }
}

function SelectRoom(){ //Option to select room for above form
  ?>
  <option value=""></option>
  <?php
  for($i = 1; $i <= 9; $i++)
  { 
    ?>
    <option value=<?php echo $i; ?> > <?php echo $i; ?> </option>
    <?php
  }
}

function searchQuery($name, $room, $day, $month, $year){
  $sql = "SELECT * FROM `bookings` WHERE ";
  if(checkdate($month, $day, $year))
  {
    $sql = $sql . "`time` >= :date AND `time` < :max_date AND";
  }
  if($name != "")
  {
    $sql = $sql . " `user` = :username AND";
  }
  if($room != "")
  {
    $sql = $sql . "`room` = :roomNo AND";
  }
  return substr($sql, 0, -3);                      //Remove the trailing AND
}

function displayEveryBooking($pagenum){//Print out the table that displays all bookings.
  $databaseConnection = getDatabaseConnection();
	try //Print out the information.
	{
    $total = 200; //Set the limit for how many results to display per page.
    $start = ($total * $pagenum);
    //Get the bookings from the database.
		$query = "SELECT * FROM `bookings` LIMIT :start, :total";
    $connection = $databaseConnection -> prepare($query);
    $connection-> bindValue( ":start", $start, PDO::PARAM_INT );
    $connection-> bindValue( ":total", $total, PDO::PARAM_INT );
    $connection-> execute();
    $result = $connection -> fetchall();
    printTable($result);
		$databaseConnection = "";//Close connection.
	}
  catch ( PDOException $e ) 
	{
		$databaseConnection = "";
		echo "Error Occured: " . $e->getMessage();
		die();//Close the connection.
	}
}

function displaySearchedBookings($query, $username, $room, $day, $month, $year){//Print out the table that displays all bookings.
  $databaseConnection = getDatabaseConnection();
  $sql = $query; 
  try //Print out the information.
  {
    //Get the bookings from the database.
    $connection = $databaseConnection -> prepare($sql);
    if(checkdate($month, $day, $year)){
      $date = $year . "-" . $month . "-" . $day . " " . "00:0:0";
      $date = strtotime($date);
      $max_date = $date + DAY;
      $date = date("Y-m-d H:i:s", $date);
      $max_date = date("Y-m-d H:i:s", $max_date);
      $connection-> bindValue( ":date", $date, PDO::PARAM_STR );
      $connection-> bindValue( ":max_date", $max_date, PDO::PARAM_STR );
    }
    if($username != "")
    {
      $connection-> bindValue( ":username", $username, PDO::PARAM_STR );
    }
    if($room != "")
    {
      $connection-> bindValue( ":roomNo", $room, PDO::PARAM_INT);
    }
    $connection-> execute();
    $result = $connection -> fetchall();
    printTable($result);
    $databaseConnection = "";//Close connection.
  }
  catch ( PDOException $e ) 
  {
    $databaseConnection = "";
    echo "Error Occured: " . $e->getMessage();
    die();//Close the connection.
  }
}

function deleteBookings($variableString)
{
  $variables = explode(",", $variableString);
  if(count($variables) == 3)
  {
    $room = $variables[0];
    $username = $variables[1];
    $time = $variables[2];
    deleteBooking($username, $time, $room);
  }
  else
  {
    //error message
  }
}

function printTable($result)
{
  ?>
  <table> 
    <tr><td>Room</td><td>User</td><td>Times</td><td>Purpose</td><td>Delete Booking</td></tr>
    <?php
    foreach($result as $row)
    {
      ?>
      <tr>
        <td><? echo $row['room'] ?></td><td><? echo $row['user'] ?></td><td><?php echo $row['time'] ?></td><td><?php echo $row['purpose'] ?></td>
        <td><input type="radio" name="delete" value= <?php echo $row['room'] . "," . $row['user'] . "," . strtotime($row['time'])?>></td>
      </tr>
      <?php
    }
    ?>
    <tr><td></td><td></td><td></td><td></td><td><input type="submit" name="deleteSubmit" value="Delete" id="deleteSubmit"></input></td></tr>
    </table>
    <?php
}

displayFooter();
?>
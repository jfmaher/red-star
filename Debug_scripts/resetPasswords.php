<?php 
require_once( "../common.inc.php" );
session_start();
insertStandardHTML( "Reset Passwords" );
//displayDebugMessage();

if ( isset( $_POST["submitButton"] ) ) {
	
} 
else {
  displayForm();
}

function displayForm() {
	$databaseConnection = getDatabaseConnection();

	$sql = "SELECT * FROM `users`";
		try {
			$connection = $databaseConnection->prepare( $sql );
			$connection-> execute();
			$queryResults = $connection->fetchAll();
			$databaseConnection = "";
			foreach($queryResults as $result)
			{
				$currentUser = User::getUser($result["username"]);
				$currentUser -> debugResetPassword();
			}
		}
		catch (PDOException $e) {
			parent::disconnect( $databaseConnection );
			die ( "Invalid query: " . $e->getMessage() );
		}
}
displayFooter();
?>

<?php
require_once( "common.inc.php" );
include('calendar.inc.php');
checkedLoggedIn();
insertStandardHTML( "Heat Map of Meeting Rooms" );
if ( isset( $_POST["submitButton"] ) ) {
  createNewGroup($_POST["groupName"]);
} 

?>
	<body>
			<div>
			<table border="0" cellspacing="0" cellpadding="5"><tr valign="top">
				<td>
				 <?php
				    $year = date('Y');
				    $month = date('n');
				    $count = 0;

				    // for loop to display months
				    for($i = $month+2; $i>$month-9; $i-=2){
				      $y = $year;
				      $y2 = $year;
				      $m = $i;
				      $m2 = $i+1;
				      if($m <= 0) {
				        $m = $m + 12;
				        $y--;
				      }
				      if($m2 <= 0) {
				        $m2 = $m2 + 12;
				        $y2--;
				      }?>
				      <tr>
				      <td>
				      <?php
				      $cal = new CALENDAR($y, $m);
				      $cal->offset = $offset;
				      $cal->weekNumbers = $weeks;
				      echo $cal->create() . '<br>';
				      ?>
				      </td>
				      <td>
				      <?php
				      $cal = new CALENDAR($y2, $m2);
				      $cal->offset = $offset;
				      $cal->weekNumbers = $weeks;
				      echo $cal->create() . '<br>';
				      ?>
				      </td>
				      </tr>
				    <?php
				}

				  ?>
				</td>

			
				</table>
		

	</body>
<?php
displayFooter();
?>

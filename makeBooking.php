<?php
require_once( "common.inc.php" );
checkedLoggedIn();
checkBlacklist();
insertCalendarHTML( "Make Booking" );

if ( isset( $_POST["submitButton"] ) ) {
  if( isset($_POST["room"]) &&  isset($_POST["date"]) )
  {
    makeBooking($_POST["date"], $_POST["room"], $_POST["duration"]);
  }
  else
  {
    processForm();
  }
} 
else {
  displayForm();
}


function processForm() 
{
  $userProfile = Profile::getProfile($_SESSION["user"]->getValue("profile"));
  if(isDateValid()) {
    if(count(getFutureBookings()) < Profile::maxFutureBookings($_SESSION["user"]->getValue("profile")) || isAdminAccount($_SESSION["user"]->getValue("username"))) {
      $date = $_POST["year"] . "-" . $_POST["month"] . "-" . $_POST["day"] . " " . $_POST["hour"] . ":0:0";
      $date = strtotime($date);
      $max_date = strtotime("+" . $userProfile->getValue("Booking_limit") . " day" , time());
      if($date > time() && $date < $max_date || isAdminAccount($_SESSION["user"]->getValue("username"))) {
        displayFreeBookings($date, $_POST["duration"]);
      }
      else {
        echo "You cannot make a booking in the past or you cannot make a booking more than " . $userProfile->getValue("Booking_limit") . " days into the future." ;
        displayForm();
      }
    }
    else {
      echo "You have made the maximum number of future bookings.";
      displayForm();
    }
  }
  else {
    echo "<p>The date entered was not valid! Please try again.</p>";
    echo $_POST["year"] . "-" . $_POST["month"] . "-" . $_POST["day"];
    displayForm();
  }
}

/*
*   Function displays all the free rooms for a time period
*/
function displayFreeBookings($date, $duration)
{
  $databaseConnection = getDatabaseConnection();
  $sql = getFreeRoomQuery($date, $duration);                      // Find the free rooms for a time a duration
  $rows = $databaseConnection->query( $sql );
  $purpose = $_POST["purpose"];
  $bookingFor = $_POST["bookingFor"];
  //If there is a free room make a booking
  $date = date("Y-m-d H:i:s", $date);                             // Converts date from Unix time stamp to one that the MySQL database will accept
  if($rows -> rowCount() > 0)                                     // Display the form if there are free rooms
    {
      ?>
     <form action="makeBooking.php" method="post">
      <div style="width: 30em; padding-left: 10px;">
        <h1>Which room?</h1>
        <input type="hidden" name="purpose" id="purpose" value= "<?php echo $purpose ?>" />
        <input type="hidden" name="date" id="date" value="<?php echo $date ?>" />
        <input type="hidden" name="duration" id="duration" value=<?php echo $duration ?> />
        <input type="hidden" name="bookingFor" id="bookingFor" value=<?php echo $bookingFor ?> />
        <table>
          <tr>
            <td>Room Number</td><td>Capacity</td><td>Monitor</td><td>Select</td>
          </tr>
          <?php
      foreach ($rows as $row)
      {
        ?>
        <tr>
          <?php
            echo "<td>" . $row["number"] . "</td>" . "<td>" . $row["capacity"] . "</td>" . "<td>" . (($row["monitor"] == 0) ? "No" : "Yes") . "</td>" . "<td>";
          ?>
        <input type = "radio" name = "room" id = "room" value = <?php echo $row["number"]?> checked = "checked" />
          </td>
        </tr>
        <?php
      } 
      ?>
    </table>
        <div style="clear: both;">
            <input type="submit" name="submitButton" id="submitButton" value="Send Details" />
            <input type="reset" name="resetButton" id="resetButton" value="Reset Form" style="margin-right: 20px;" />
        </div>
      </div>
    </form>
    <?php
    }
    else 
    {
      echo "<h1>Booking failed:</h1><p>All Rooms are occupied at that time.</p>";
      displayForm();
    } 

}

/*
*     Function that receives a date and a duration and returns all of the rooms that are free during that time period
*/  
function getFreeRoomQuery($date, $duration)
{
  $tempDate = date("Y-m-d H:i:s", $date);
  $sql = "SELECT * FROM rooms WHERE rooms.number NOT IN (SELECT bookings.room FROM bookings WHERE bookings.time = '" . $tempDate . "')" ;   // Uses subquery to get rooms freeRoom
  for( $i = 1; $i < $duration; $i++)
  {
    //Converts the date into a format that MySQL will accept
    $tempDate = date("Y-m-d H:i:s", $date + ($i * HOUR));
    $sql = $sql . " AND rooms.number NOT IN (SELECT bookings.room FROM bookings WHERE bookings.time = '" . $tempDate . "')" ;
    //Retreives the list of rooms that are free at the specified time
    //The user should be presented with the list of rooms and be allowed to choose  
  }
  return $sql;
}


/*
*     Funtion that receives a date, a duration and room number and will make a booking for this time
*/
function makeBooking($date, $roomNumber, $duration) 
{
  $purpose = $_POST["purpose"];           
  $bookingFor = $_POST["bookingFor"];     
  $username = $_SESSION["user"]->getValue("username");
  if($bookingFor == -1)                                     // If -1, the booking is for a groups
  {
    $groupBooking = 0;
    $groupNumber = NULL;
  }
  else
  {
    $groupBooking = 1;
    $groupNumber = $bookingFor;
  }
  $sql = "INSERT INTO bookings (`time`, `user`, `area`, `room`, `group_id`, `purpose`, `group_booking`) VALUES (:date, :username, :area, :freeRoom , :groupNumber, :purpose, :groupBooking)" ; //Clears table
  for($i = 0; $i < $duration; $i++)
  {
    $databaseConnection = getDatabaseConnection();
    try {
      $unix_time = strtotime($date) + ($i * HOUR);
      $connection = $databaseConnection->prepare( $sql );
      $connection-> bindValue( ":date", date("Y-m-d H:i:s", $unix_time), PDO::PARAM_STR );
      $connection-> bindValue( ":username", $username, PDO::PARAM_STR );
      $connection-> bindValue( ":area", AREA_GLASS_MEETING_ROOMS, PDO::PARAM_INT); 
      $connection-> bindValue( ":freeRoom", $roomNumber, PDO::PARAM_INT );
      $connection-> bindValue( ":groupNumber", $groupNumber, PDO::PARAM_INT );
      $connection-> bindValue( ":purpose", $purpose, PDO::PARAM_STR );
      $connection-> bindValue( ":groupBooking", $groupBooking, PDO::PARAM_INT );
      $connection-> execute();
      echo "<h1>Booking Successful</h1><p>You booked room " . $roomNumber . " for " . date("Y-m-d H:i:s", $unix_time) . "</p>";
    }
    catch (PDOException $e) {
      $databaseConnection = "";            //closes connection
      if ($e->errorInfo[1] == 1062) {
        //echo $e->getMessage();
        echo "<h1>Booking failed:</h1><p>You already have a booking for that time or the room is unavailble at that time</p>";
        displayForm();
      }
      else {
          echo "<h1>Booking failed:</h1><p>" . $e->getMessage() . "</p>";
      }                    
    }
    $databaseConnection = "";                       //closes connection 
  }
}


/*
*     Function tha displays the date selection form
*/
function displayForm() 
{
  $currentBookings = getFutureBookings();
  // If the user has any current bookings, this displays a table of them
  if(count($currentBookings) > 0)  {
    ?>
      <h1>Current Bookings:</h1>
      <?php
        echo "<table>"; 
        echo "<td>Time</td><td>Username</td><td>Room Number</td><td>Purpose</td><td>Group Booking</td>";
        foreach ($currentBookings as $booking)  {
          echo "<tr>";
          echo "<td>" . $booking["time"] . "</td>" . "<td>" . $booking["user"] . "</td>" . "<td>" . $booking["room"] . "</td>" . "<td>" . $booking["purpose"] . "</td>" . "<td>" . ($booking["group_booking"] ? "Yes": "No") . "</td>";
          echo "</tr>";
        }
        echo "</table>"; 
  }
  else  {
    echo "<h1>Currently no bookings:</h1>";
  }
  ?>
    <h1>Make a Booking:</h1>
        <?php displayDateOptions("makeBooking.php");              // Outputs the display date options
        displayHours(); ?>
        <p>
          <label for="purpose">Purpose</label>
          <input type="text" name="purpose" id="purpose" maxlength="129"/></td>          
        </p>
        <label for="duration">Duration</label>
        <select name="duration" size="1">
        <?php

        for($i = 1; $i <= (Profile::maxFutureBookings($_SESSION["user"]->getValue("profile"))); $i++)         // Prints a list of durations that conforms with the users max 
        {                                                                                                     // number of possible bookings
          echo '<option value="' . $i . '">' . $i .'</option>';
        }
        ?>
         </select>
         <p><label for="bookingFor">Who is the booking for?</label>
        <select name="bookingFor" size="1">
        <option value=-1>Yourself</option>                                                <!-- By default -1, is a booking for an individual, else it's the group number -->
        <?php
        $groups = getUsersGroups($_SESSION["user"]->getValue("username"));
        foreach($groups as $group)
        {
          echo '<option value="' . $group['group_id'] . '">' . $group['name'] .'</option>';
        }
        ?>
         </select>
       </p>
        <div style="clear: both;">
          <input type="submit" name="submitButton" id="submitButton" value="Send Details" />
          <input type="reset" name="resetButton" id="resetButton" value="Reset Form" style="margin-right: 20px;" />
        </div>
      </div>
    </form>
<?php
}

displayFooter();
?>
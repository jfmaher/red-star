<?php
// Page that defines any constants used in the system
define("DB_NAME", "mysql:dbname=bookingdata");
define("DB_USERNAME", "root");
define("DB_PASSWORD", "root");
define("COUNT", 1000);
define("KEY_LENGTH", 32);
define("RAW_OUTPUT", false);
define ("DAY", 86400);
define("HOUR", 3600);
define("MIN", 60);
define("AREA_GLASS_MEETING_ROOMS", 0);
?>